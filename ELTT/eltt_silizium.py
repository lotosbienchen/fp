import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sp
from scipy.optimize import curve_fit
import scipy.odr

from uncertainties import unumpy

start_ex=430
end_ex=460
end_exb=460
start_in=800
end_in=1760

Si_data = np.genfromtxt('20181213_Hilgenfeld_Keller/Cu_Si_Koch-Ray_01_changed.dat', skip_header=2)
T_Si = Si_data[0:,1]
R_Si = Si_data[0:,4]

red_R_theo = np.genfromtxt('red_widerstand_theo.csv', skip_header=1)
red_T_theo = red_R_theo[0:,1]
red_R_D_theo = red_R_theo[0:,2]

s_mas = 10
s_mess = 61.75
ds_mess = 1
k = s_mas/s_mess
dk = np.abs(s_mas/s_mess**2*ds_mess)

l_mess = 33.85
l = k*l_mess
dl_mess = 1
dl = np.sqrt((k*dl_mess)**2+(l*dk)**2)

b_mess = 60
b = k*b_mess
db_mess = 1
db = np.sqrt((k*db_mess)**2+(b*dk)**2)

d_mess = 0.55
dd_mess = 0.1

R_Si_spez = R_Si*b*d_mess*1000/l

dR_Si_spez = np.sqrt((R_Si*d_mess*1000*db/l)**2+(R_Si*b*1000*dd_mess/l)**2+(R_Si*d_mess*b*1000*dl/l)**2)

RI_array = unumpy.uarray(R_Si_spez, dR_Si_spez)

el_L = unumpy.uarray(1/unumpy.nominal_values(RI_array), np.abs(unumpy.std_devs(RI_array)*unumpy.nominal_values(RI_array)**(-2)))
n = 2.8e18
e = 1.602e-19
mu = unumpy.uarray(unumpy.nominal_values(el_L)/n/e, unumpy.std_devs(el_L)/n/e)

y_Data_0 = unumpy.uarray(np.log(unumpy.nominal_values(el_L)/T_Si), unumpy.std_devs(el_L)/unumpy.nominal_values(el_L))

def linear_fit1(B, x):
    return(B[0]*x + B[1])

def linear_fit2(x, E_A, A):
    return(E_A*x + A)

data0_exfit1 = scipy.odr.RealData(1/T_Si[start_ex:end_ex], unumpy.nominal_values(y_Data_0[start_ex:end_ex]), sy=unumpy.std_devs(y_Data_0[start_ex:end_ex]))
ex1_model = scipy.odr.Model(linear_fit1)
odr_ex1 = scipy.odr.ODR(data0_exfit1, ex1_model, beta0=[-200, -17])
odr_ex1_out = odr_ex1.run()
beta_ex1 = unumpy.uarray(odr_ex1_out.beta, odr_ex1_out.sd_beta)
y_01_new = ex1_model.fcn(odr_ex1_out.beta, 1/T_Si[start_ex:end_ex])
print('=======')
print('E_A fuer ex. ohne Faktor', beta_ex1[0]*2*8.617e-5)

data0_exfit2 = scipy.odr.RealData(1/T_Si[start_ex:end_in], unumpy.nominal_values(y_Data_0[start_ex:end_in]), sy=unumpy.std_devs(y_Data_0[start_ex:end_in]))
ex2_model = scipy.odr.Model(linear_fit1)
odr_ex2 = scipy.odr.ODR(data0_exfit2, ex2_model, beta0=[-200, -17])
odr_ex2_out = odr_ex2.run()
beta_ex2 = unumpy.uarray(odr_ex2_out.beta, odr_ex2_out.sd_beta)
y_01_new1 = ex2_model.fcn(odr_ex2_out.beta, 1/T_Si[start_ex:end_in])
print('=======')
print('E_A fuer nur ex ohne Faktor', beta_ex2[0]*2*8.617e-5)

data0_infit1 = scipy.odr.RealData(1/T_Si[start_in:end_in], unumpy.nominal_values(y_Data_0[start_in:end_in]), sy=unumpy.std_devs(y_Data_0[start_in:end_in]))
in1_model = scipy.odr.Model(linear_fit1)
odr_in1 = scipy.odr.ODR(data0_infit1, in1_model, beta0=[-200, -17])
odr_in1_out = odr_in1.run()
beta_in1 = unumpy.uarray(odr_in1_out.beta, odr_in1_out.sd_beta)
y_02_new = in1_model.fcn(odr_in1_out.beta, 1/T_Si[start_in:end_in])
print('=======')
print('E_A fuer in ohne Faktor', beta_in1[0]*2*8.617e-5)

y_Data_1 = unumpy.uarray(np.log(unumpy.nominal_values(el_L)/T_Si**(3/2)), unumpy.std_devs(el_L)/unumpy.nominal_values(el_L))

def linear_fit1(B, x):
    return(B[0]*x + B[1])

def linear_fit2(x, E_A, A):
    return(E_A*x + A)

data1_exfit1 = scipy.odr.RealData(1/T_Si[start_ex:end_ex], unumpy.nominal_values(y_Data_1[start_ex:end_ex]), sy=unumpy.std_devs(y_Data_1[start_ex:end_ex]))
ex1_model = scipy.odr.Model(linear_fit1)
odr_ex1 = scipy.odr.ODR(data1_exfit1, ex1_model, beta0=[-200, -17])
odr_ex1_out = odr_ex1.run()
beta_ex1 = unumpy.uarray(odr_ex1_out.beta, odr_ex1_out.sd_beta)
y_11_new = ex1_model.fcn(odr_ex1_out.beta, 1/T_Si[start_ex:end_ex])
print('=======')
print('E_A fuer ex mit Faktor T^3/2', beta_ex1[0]*2*8.617e-5)

data1_exfit2 = scipy.odr.RealData(1/T_Si[start_ex:end_in], unumpy.nominal_values(y_Data_1[start_ex:end_in]), sy=unumpy.std_devs(y_Data_1[start_ex:end_in]))
ex2_model = scipy.odr.Model(linear_fit1)
odr_ex2 = scipy.odr.ODR(data1_exfit2, ex2_model, beta0=[-200, -17])
odr_ex2_out = odr_ex2.run()
beta_ex2 = unumpy.uarray(odr_ex2_out.beta, odr_ex2_out.sd_beta)
y_11_new1 = ex2_model.fcn(odr_ex2_out.beta, 1/T_Si[start_ex:end_in])
print('=======')
print('E_A fuer nur ex mit Faktor T^3/2', beta_ex2[0]*2*8.617e-5)

data1_infit1 = scipy.odr.RealData(1/T_Si[start_in:end_in], unumpy.nominal_values(y_Data_1[start_in:end_in]), sy=unumpy.std_devs(y_Data_1[start_in:end_in]))
in1_model = scipy.odr.Model(linear_fit1)
odr_in1 = scipy.odr.ODR(data1_infit1, in1_model, beta0=[-200, -17])
odr_in1_out = odr_in1.run()
beta_in1 = unumpy.uarray(odr_in1_out.beta, odr_in1_out.sd_beta)
y_12_new = in1_model.fcn(odr_in1_out.beta, 1/T_Si[start_in:end_in])
print('=======')
print('E_A fuer in mit Faktor T^3/2', beta_in1[0]*2*8.617e-5)

y_Data_2 = unumpy.uarray(np.log(unumpy.nominal_values(el_L)/T_Si**(3)), unumpy.std_devs(el_L)/unumpy.nominal_values(el_L))

data2_exfit1 = scipy.odr.RealData(1/T_Si[start_ex:end_exb], unumpy.nominal_values(y_Data_2[start_ex:end_exb]), sy=unumpy.std_devs(y_Data_2[start_ex:end_exb]))
ex1_model = scipy.odr.Model(linear_fit1)
odr_ex1 = scipy.odr.ODR(data2_exfit1, ex1_model, beta0=[-200, -30])
odr_ex1_out = odr_ex1.run()
beta_ex1 = unumpy.uarray(odr_ex1_out.beta, odr_ex1_out.sd_beta)
y_21_new = ex1_model.fcn(odr_ex1_out.beta, 1/T_Si[start_ex:end_ex])
print('=======')
print('E_A fuer ex mit Faktor T^3', beta_ex1[0]*2*8.617e-5)

data2_exfit2 = scipy.odr.RealData(1/T_Si[start_ex:end_in], unumpy.nominal_values(y_Data_2[start_ex:end_in]), sy=unumpy.std_devs(y_Data_2[start_ex:end_in]))
ex2_model = scipy.odr.Model(linear_fit1)
odr_ex2 = scipy.odr.ODR(data2_exfit2, ex2_model, beta0=[-200, -17])
odr_ex2_out = odr_ex2.run()
beta_ex2 = unumpy.uarray(odr_ex2_out.beta, odr_ex2_out.sd_beta)
y_21_new1 = ex2_model.fcn(odr_ex2_out.beta, 1/T_Si[start_ex:end_in])
print('=======')
print('E_A fuer nur ex mit Faktor T^3', beta_ex2[0]*2*8.617e-5)

data2_infit1 = scipy.odr.RealData(1/T_Si[start_in:end_in], unumpy.nominal_values(y_Data_2[start_in:end_in]), sy=unumpy.std_devs(y_Data_2[start_in:end_in]))
in1_model = scipy.odr.Model(linear_fit1)
odr_in1 = scipy.odr.ODR(data2_infit1, in1_model, beta0=[-200, -17])
odr_in1_out = odr_in1.run()
beta_in1 = unumpy.uarray(odr_in1_out.beta, odr_in1_out.sd_beta)
y_22_new = in1_model.fcn(odr_in1_out.beta, 1/T_Si[start_in:end_in])
print('=======')
print('E_A fuer in mit Faktor T^3', beta_in1[0]*2*8.617e-5)

plt.figure(0)
plt.xlabel(r'$T^{-1}$ [$K^{-1}$]')
plt.ylabel(r'ln($\sigma$)')
plt.plot(1/T_Si[start_ex:], unumpy.nominal_values(y_Data_0[start_ex:]), ls='', marker='.', color='b')
#plt.plot(1/T_Si[start_ex:end_ex], unumpy.nominal_values(y_Data_0[start_ex:end_ex]), ls='', marker='.', color='r')
plt.plot(1/T_Si[start_ex:end_ex], y_01_new, ls='--', color='r', lw=2)
#plt.plot(1/T_Si[start_in:end_in], unumpy.nominal_values(y_Data_0[start_in:end_in]), ls='', marker='.', color='r')
plt.plot(1/T_Si[start_in:end_in], y_02_new, ls='--', color='r', lw=2)
plt.savefig('lnsigma_mit_fit.png')

plt.figure(1)
plt.xlabel(r'$T^{-1}$ [$K^{-1}$]')
plt.ylabel(r'ln($\sigma\cdot$ Faktor)')
plt.plot(1/T_Si[start_ex:], unumpy.nominal_values(y_Data_0[start_ex:]), ls='', marker='.', color='r', label=r'ln($\sigma$)')
plt.plot(1/T_Si[start_ex:], unumpy.nominal_values(y_Data_1[start_ex:]), ls='', marker='.', color='b', label=r'ln$\left(\frac{\sigma}{T^{3/2}}\right)$')
#plt.plot(1/T_Si[start_ex:end_ex], unumpy.nominal_values(y_Data_1[start_ex:end_ex]), ls='', marker='.', color='r')
plt.plot(1/T_Si[start_ex:end_ex], y_11_new, ls='--', color='r', lw=2)
#plt.plot(1/T_Si[start_in:end_in], unumpy.nominal_values(y_Data_1[start_in:end_in]), ls='', marker='.', color='r')
plt.plot(1/T_Si[start_in:end_in], y_12_new, ls='--', color='r', lw=2)
plt.plot(1/T_Si[start_ex:], unumpy.nominal_values(y_Data_2[start_ex:]), ls='', marker='.', color='orange', label=r'ln$\left(\frac{\sigma}{T^3}\right)$')
#plt.plot(1/T_Si[start_ex:end_ex], unumpy.nominal_values(y_Data_2[start_ex:end_ex]), ls='', marker='.', color='r')
plt.plot(1/T_Si[start_ex:end_ex], y_21_new, ls='--', color='r', lw=2)
#plt.plot(1/T_Si[start_in:end_in], unumpy.nominal_values(y_Data_2[start_in:end_in]), ls='', marker='.', color='r')
plt.plot(1/T_Si[start_in:end_in], y_22_new, ls='--', color='r', lw=2)
plt.legend()
plt.savefig('Silizium_alternativ.png')


plt.figure(2)
plt.xlabel(r'$T^{-1}$ [$K^{-1}$]')
plt.ylabel(r'ln($\sigma\cdot$ Faktor)')
plt.plot(1/T_Si[start_ex:], unumpy.nominal_values(y_Data_0[start_ex:]), ls='', marker='.', color='r')
plt.plot(1/T_Si[start_ex:], unumpy.nominal_values(y_Data_1[start_ex:]), ls='', marker='.', color='b')
plt.plot(1/T_Si[start_ex:], unumpy.nominal_values(y_Data_2[start_ex:]), ls='', marker='.', color='orange')
plt.plot(1/T_Si[start_ex:end_in], y_01_new1, ls='--', color='g', lw=2)
plt.plot(1/T_Si[start_ex:end_in], y_11_new1, ls='--', color='r', lw=2)
plt.plot(1/T_Si[start_ex:end_in], y_21_new1, ls='--', color='b', lw=2)
plt.savefig('Silizium_mal_anders.png')

plt.show()
