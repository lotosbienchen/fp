import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sp
from scipy.optimize import curve_fit
import scipy.odr

from uncertainties import unumpy

"""
Daten
"""

Cu_Si_data = np.genfromtxt('20181213_Hilgenfeld_Keller/Cu_Si_Koch-Ray_01_changed.dat', skip_header=2)
T_Cu_Si = Cu_Si_data[0:,1]
R_Cu = Cu_Si_data[0:,2]
R_Si = Cu_Si_data[0:,4]

red_R_theo = np.genfromtxt('red_widerstand_theo.csv', skip_header=1)
red_T_theo = red_R_theo[0:,1]
red_R_D_theo = red_R_theo[0:,2]

Nb_data = np.genfromtxt('20181213_Hilgenfeld_Keller/Niob_Nov2018.dat', skip_header=2)
T_Nb = Nb_data[0:,1]
R_Nb = Nb_data[0:,2]
print(T_Nb)
print(R_Nb)
"""
Niob
"""
B_i = 0.03

T_c = (T_Nb[4109]+T_Nb[4110])/2
print(T_c)
print(T_Nb[4109])
print(T_Nb[4110])
T_c1 = np.mean(T_Nb[4106:4114])

print(T_c1)

R_Nb_rest = np.mean(R_Nb[4000:4106])
R_Nb_red = R_Nb-R_Nb_rest
R_Nb_spez = R_Nb*1*0.7*1000/15
el_L_Nb = 1/R_Nb_spez
T_min = np.min(T_Nb)
Bc0 = B_i/(1-(13.2/12.2)**2)
print(Bc0)
DBc0 = np.sqrt((0.002/(1-(13.2/12.2)**2))**2 + (2*B_i*12.2**2*13.2*0.2/((12.2**2-13.2**2)**2))**2 + (2*B_i*13.2**2*12.2*0.1/((12.2**2-13.2**2)**2))**2)
print(DBc0)
B_c2 = -Bc0*(1-(T_Nb[4400:7000]/12.2)**2)
DB_c2 = np.sqrt((DBc0*(1-(T_Nb[4400:7000]/12.2)**2))**2 + (0.1*Bc0*2*T_Nb[4400:7000]**2/T_c**3)**2)
Bc2 = unumpy.uarray(B_c2, DB_c2)
print(DB_c2)
S = np.polyfit(T_Nb[6900:7000], B_c2[2500:2600], 1)
print(S)
f_S = np.poly1d(S)
print(f_S)

cohlen = np.sqrt(-2.07e-15/(2*np.pi*12.2*S[0]))
Dcohlen = np.sqrt((0.5*np.sqrt(-2.07e-15/(2*np.pi*S[0]))*0.1/(12.2)**(3/2))**2)
print(cohlen, Dcohlen)
cohlen_2 = np.sqrt(-2.07e-15/(2*np.pi*Bc0))
Dcohlen_2 = np.sqrt((0.5*np.sqrt(2.07e-15/(2*np.pi))*DBc0/(-Bc0)**(3/2))**2)
print(cohlen_2, Dcohlen_2)

l_1 = cohlen**2/(39e-9)
dl_1 = 2*cohlen*Dcohlen/39e-9
l_2 = cohlen_2**2/(39e-9)
dl_2 = 2*Dcohlen_2*cohlen_2/39e-9
print(l_1, dl_1, l_2, dl_2)

def Nbgb_fit1(B, x):
    return(1.17*B[0]*x/B[1] - 0.17*B[0])

def Nbgb_fit2(x, a, b):
    return(1.17*a*x/b - 0.17*a)

def linear_fit(B, x):
    return(B[0]*x - B[1])

data_Nbgbfit = scipy.odr.RealData(T_Nb[0:3200], R_Nb_red[0:3200])
Nbgb_model = scipy.odr.Model(Nbgb_fit1)
odr_Nbgb = scipy.odr.ODR(data_Nbgbfit, Nbgb_model, beta0=[1, 270])
odr_Nbgb_out = odr_Nbgb.run()
Nbbeta1 = unumpy.uarray(odr_Nbgb_out.beta, odr_Nbgb_out.sd_beta)
T_Nb_debye = unumpy.nominal_values(Nbbeta1[1])
print(Nbbeta1)

data_linearfit = scipy.odr.RealData(T_Nb[6900:7000], B_c2[2500:2600], sy=unumpy.std_devs(Bc2[2500:2600]))
linear_model = scipy.odr.Model(linear_fit)
odr_linear = scipy.odr.ODR(data_linearfit, linear_model, beta0=[1, 270])
odr_linear_out = odr_linear.run()
linbeta1 = unumpy.uarray(odr_linear_out.beta, odr_linear_out.sd_beta)
print(linbeta1)


T_Nb_new = np.linspace(T_Nb[0], T_Nb[3200], len(T_Nb[0:3200]))
fitted_R_Nb_T = Nbgb_model.fcn(odr_Nbgb_out.beta, T_Nb_new)
print(T_Nb_debye)

R_Nb_debye = Nbgb_fit1(odr_Nbgb_out.beta, T_Nb_debye)

plt.figure(13)
plt.xlabel('T [K]')
plt.ylabel(r'R [$\Omega$]')
plt.plot(T_Nb, R_Nb, marker='.', ls='')
plt.plot(T_Nb[2000:2700], R_Nb[2000:2700], marker='.', ls='', color='r')
plt.savefig('Niob_Messung.png')

plt.figure(14)
plt.xlabel('T [K]')
plt.ylabel(r'R [$\Omega$]')
plt.plot(T_Nb[0:4106], R_Nb[0:4106] - R_Nb_rest, marker='.', ls='')
plt.plot(T_Nb[0:3200], R_Nb[0:3200] - R_Nb_rest, marker='.', ls='', color='r')

plt.figure(15)
plt.xlabel('T [K]')
plt.ylabel(r'R [$\Omega$]')
plt.plot(T_Nb[4000:7279], R_Nb[4000:7279], marker='.', ls='')
plt.plot(T_Nb[4000:7279], R_Nb[4000:7279], marker='', ls='-', lw=0.5, color='grey')
#plt.plot(T_Nb[4400:7000], R_Nb[4400:7000], marker='.', ls='', color='r')
plt.axvline(x=T_c, color='green', alpha=0.8, ls='-.', label=r'$T_c$ im magn. Feld')
plt.axvline(x=12.2, color='red', alpha=0.6, ls='-.', label=r'$T_c$ im magn. NullFeld')
plt.axvline(x=13.2, color='orange', alpha=0.4, ls='-.', label=r'Beginn normalleit. Bereich')
plt.legend()
plt.savefig('Niob_T_c.png')

plt.figure(16)
plt.xlabel('T [K]')
plt.ylabel(r'B [T]')
plt.plot(T_Nb[4400:7000], B_c2, ls='', marker='.')
plt.plot(T_Nb[6500:7000], f_S(T_Nb[6500:7000]), ls='--', color='r', lw='2')
plt.savefig('B_crit2.png')


'''
Kupfer
'''
R_Cu_300 = R_Cu[6747:6814]
T_300 = T_Cu_Si[6747:6814]
#print(T_300)


R_Cu_300_mean = np.mean(R_Cu_300)
#print(R_Cu_300_mean)

std_R_Cu_300 = np.std(R_Cu_300)
err_R_Cu_300 = std_R_Cu_300/np.sqrt(len(R_Cu_300))
#print(err_R_Cu_300)

start = 50
max = 355
#max = len(R_Cu) - 1
number = max-start
n = np.linspace(start, max, number, dtype=int)
R_rest = np.mean(R_Cu[start:max])

std_R_rest = np.std(R_Cu[start:max])
err_R_rest = std_R_rest/np.sqrt(len(R_Cu[start:max]))
#print(err_R_rest)

RRR = R_Cu_300_mean/R_rest
dRRR = np.sqrt((err_R_Cu_300/R_rest)**2+(err_R_Cu_300*err_R_rest/(R_rest)**2)**2)
#print(R_rest)
#print(RRR)
#print(dRRR)
T_min = np.min(T_Cu_Si)
#print(T_min)
R_red = R_Cu - R_rest

R_Cu_spez = R_Cu*np.pi*0.07**2/4
#print(R_Cu_spez)

def gb_fit1(B, x):
    return(1.17*B[0]*x/B[1] - 0.17*B[0])

def gb_fit2(x, a, b):
    return(1.17*a*x/b - 0.17*a)

data_gbfit = scipy.odr.RealData(T_Cu_Si[500:], R_red[500:])
gb_model = scipy.odr.Model(gb_fit1)
odr_gb = scipy.odr.ODR(data_gbfit, gb_model, beta0=[1, 345])
odr_gb_out = odr_gb.run()
beta1 = unumpy.uarray(odr_gb_out.beta, odr_gb_out.sd_beta)
T_debye = unumpy.nominal_values(beta1[1])

T_new = np.linspace(T_Cu_Si[500], T_Cu_Si[-1], len(T_Cu_Si[500:]))
fitted_R_T = gb_model.fcn(odr_gb_out.beta, T_new)

R_debye = gb_fit1(odr_gb_out.beta, 388.39)
print(R_Nb_debye)

plt.figure(1)
plt.xlabel(r'$T$ [K]')
plt.ylabel(r'$R$ [$\Omega$]')
plt.plot(T_Cu_Si, R_Cu, marker='.', ls='')
plt.plot(T_Cu_Si[475:525], R_Cu[475:525], marker='.', ls='', color='r')
plt.savefig('Kupfermessung.png')


plt.figure(2)
plt.xlabel(r'$T$ [K]')
plt.ylabel(r'$R$ [$\Omega$]')
plt.plot(T_Cu_Si, R_Cu, marker='.', ls='', label='Kupfermessung')
plt.axhline(y=R_rest, color='r', ls='--', label='Restwiderstand')
plt.legend()
plt.savefig('restwiderstand.png')

plt.figure(3)
plt.xlabel(r'$T$ [K]')
plt.ylabel(r'$R_T$ [$\Omega$]')
plt.plot(T_Cu_Si, R_red, marker='.', ls='', label=r'$R_T$')
plt.plot(T_new, fitted_R_T, ls='--', color='r', lw=4, label='Grüneisen-Borelius-Fit')
plt.legend()
plt.savefig('R_T.png')
#
plt.figure(4)
plt.xlabel(r'$T$ [K]')
plt.ylabel(r'$\rho$ [$\Omega\frac{mm^2}{m}$]')
plt.plot(T_Cu_Si, R_Cu_spez, ls='', marker='.', label='spez. Widerstand')
plt.axvline(x=293.15, color='r', ls='-.', label=r'$T$=293.15 K')
plt.legend()
plt.savefig('spezwiderstand.png')

plt.figure(5)
plt.xlabel(r'$T/\Theta_D$')
plt.ylabel(r'$R/R(\Theta_D)$')
plt.plot(T_Cu_Si/T_debye, R_red/R_debye, ls='', marker='.', ms=0.8, label=r'$R/R(\Theta_D)$')
plt.plot(red_T_theo[0:2], red_R_D_theo[0:2], ls='', marker='^', color='r', label=r'Na$_{theo}$')
plt.plot(red_T_theo[2:5], red_R_D_theo[2:5], ls='', marker='8', color='r', label=r'Au$_{theo}$')
plt.plot(red_T_theo[5:9], red_R_D_theo[5:9], ls='', marker='s', color='g', label=r'Cu$_{theo}$')
plt.plot(red_T_theo[9:13], red_R_D_theo[9:13], ls='', marker='P', color='r', label=r'Al$_{theo}$')
plt.plot(red_T_theo[13:16], red_R_D_theo[13:16], ls='', marker='*', color='r', label=r'Ni$_{theo}$')
plt.legend()
plt.savefig('vergleich_mess_theo.png')
'''
Silicium
'''

s_mas = 10
s_mess = 61.75
ds_mess = 1
k = s_mas/s_mess
dk = np.abs(s_mas/s_mess**2*ds_mess)

l_mess = 33.85
l = k*l_mess
dl_mess = 1
dl = np.sqrt((k*dl_mess)**2+(l*dk)**2)

b_mess = 60
b = k*b_mess
db_mess = 1
db = np.sqrt((k*db_mess)**2+(b*dk)**2)

d_mess = 0.55
dd_mess = 0.1

#print(l,b,d_mess)

R_Si_spez = R_Si*b*d_mess*1000/l
#print(R_Si_spez)
dR_Si_spez = np.sqrt((R_Si*d_mess*1000*db/l)**2+(R_Si*b*1000*dd_mess/l)**2+(R_Si*d_mess*b*1000*dl/l)**2)
#print(dR_Si_spez)

RI_array = unumpy.uarray(R_Si_spez, dR_Si_spez)
#print(RI_array)

el_L = unumpy.uarray(1/R_Si_spez, np.abs(dR_Si_spez/(R_Si_spez)**2))

#print(el_L)
n = 2.8e18
e = 1.602e-19
mu = unumpy.nominal_values(el_L)/n/e

def ex_fit1(B, T):
    return(-B[0]/T + B[1])

def ex_fit2(T, A, E_A):
    return(-E_A/T + A)

def ex_fit3(B, T):
    return(B[0]/T + B[1])

print(T_Cu_Si[1900])

data_exfit1 = scipy.odr.RealData(T_Cu_Si[430:455], np.log(unumpy.nominal_values(el_L[430:455])), sy=unumpy.std_devs(el_L)/unumpy.nominal_values(el_L))
ex_model = scipy.odr.Model(ex_fit1)
odr_ex = scipy.odr.ODR(data_exfit1, ex_model, beta0=[206, -11])
odr_ex_out = odr_ex.run()
beta_ex = unumpy.uarray(odr_ex_out.beta, odr_ex_out.sd_beta)
#print(beta_ex)

data_exfit3 = scipy.odr.RealData(T_Cu_Si[430:455], np.log(unumpy.nominal_values(el_L[430:455])/(T_Cu_Si[430:455]**(3))), sy=unumpy.std_devs(el_L)/unumpy.nominal_values(el_L))
ex3_model = scipy.odr.Model(ex_fit3)
odr_ex3 = scipy.odr.ODR(data_exfit3, ex3_model, beta0=[-206, -30])
odr_ex3_out = odr_ex3.run()
beta_ex3 = unumpy.uarray(odr_ex3_out.beta, odr_ex3_out.sd_beta)

data2_exfit = scipy.odr.RealData(T_Cu_Si[430:455], np.log(unumpy.nominal_values(el_L[430:455])/(T_Cu_Si[430:455]**(2/3))), sy=unumpy.std_devs(el_L)/unumpy.nominal_values(el_L))
ex_model = scipy.odr.Model(ex_fit1)
odr2_ex = scipy.odr.ODR(data2_exfit, ex_model, beta0=[206, -11])
odr2_ex_out = odr2_ex.run()
beta2_ex = unumpy.uarray(odr2_ex_out.beta, odr2_ex_out.sd_beta)

E_A = beta_ex*2*8.617e-5
print(E_A)
print(beta_ex)
E_A3 = beta_ex3*2*8.617e-5
print(E_A3)
E2_A3 = beta2_ex*2*8.617e-5
print(E2_A3)
print(beta2_ex)

exopt, excov = curve_fit(ex_fit2, T_Cu_Si[430:455], np.log(unumpy.nominal_values(el_L[430:455])))

A, E_A = exopt
#print(exopt)
x = np.linspace(T_Cu_Si[430], T_Cu_Si[455], len(T_Cu_Si[430:455]))

y = ex_fit2(x, unumpy.nominal_values(beta_ex[1]), unumpy.nominal_values(beta_ex[0]))
y3 = ex3_model.fcn(odr_ex3_out.beta, x)
y23 = ex_fit2(x, unumpy.nominal_values(beta2_ex[1]), unumpy.nominal_values(beta2_ex[0]))
y32 = ex_model.fcn(odr2_ex_out.beta, x)

fitted_el_L_Ed = gb_model.fcn(odr_ex_out.beta, x)

def infit1(B, T):
    return(-B[0]/T + B[1])


data_infit1 = scipy.odr.RealData(T_Cu_Si[800:1600], np.log(unumpy.nominal_values(el_L[800:1600])), sy=unumpy.std_devs(el_L)/unumpy.nominal_values(el_L))
in_model = scipy.odr.Model(infit1)
odr_in = scipy.odr.ODR(data_infit1, in_model, beta0=[206, -11])
odr_in_out = odr_in.run()
beta_in = unumpy.uarray(odr_in_out.beta, odr_in_out.sd_beta)
x_in = np.linspace(T_Cu_Si[800], T_Cu_Si[1600], len(T_Cu_Si[800:1600]))
y_in = in_model.fcn(odr_in_out.beta, x_in)

data2_infit1 = scipy.odr.RealData(T_Cu_Si[800:1600], np.log(unumpy.nominal_values(el_L[800:1600])/(T_Cu_Si[800:1600]**(3/2))), sy=unumpy.std_devs(el_L)/unumpy.nominal_values(el_L))
in_model = scipy.odr.Model(infit1)
odr2_in = scipy.odr.ODR(data2_infit1, in_model, beta0=[206, -11])
odr2_in_out = odr_in.run()
beta2_in = unumpy.uarray(odr2_in_out.beta, odr2_in_out.sd_beta)
x_in = np.linspace(T_Cu_Si[800], T_Cu_Si[1600], len(T_Cu_Si[800:1600]))
y_in = in_model.fcn(odr_in_out.beta, x_in)
y2_in = in_model.fcn(odr2_in_out.beta, x_in)

data3_infit1 = scipy.odr.RealData(T_Cu_Si[800:1600], np.log(unumpy.nominal_values(el_L[800:1600])/(T_Cu_Si[800:1600]**(3))), sy=unumpy.std_devs(el_L)/unumpy.nominal_values(el_L))
in_model = scipy.odr.Model(infit1)
odr3_in = scipy.odr.ODR(data3_infit1, in_model, beta0=[206, -11])
odr3_in_out = odr3_in.run()
beta3_in = unumpy.uarray(odr3_in_out.beta, odr3_in_out.sd_beta)
x_in = np.linspace(T_Cu_Si[800], T_Cu_Si[1600], len(T_Cu_Si[800:1600]))
y3_in = in_model.fcn(odr3_in_out.beta, x_in)

E_A_in = beta_in*2*8.617e-5
print(E_A_in)
E_A2_in = beta2_in*2*8.617e-5
print(E_A2_in)
E_A3_in = beta3_in*2*8.617e-5
print(E_A3_in)


plt.figure(6)
plt.xlabel(r'$T$ [K]')
plt.ylabel(r'$R$ [$\Omega$]')
plt.plot(T_Cu_Si, R_Si, ls='', marker='.')
#plt.plot(T_Cu_Si[0:431], R_Si[0:431], ls='', marker='.', color='r')
plt.savefig('siliziummessung.png')

plt.figure(7)
plt.xlabel(r'$T^{-1}$ [$K^{-1}$]')
plt.ylabel(r'ln($\sigma$)')
plt.plot(1/T_Cu_Si, np.log(unumpy.nominal_values(el_L)), ls='', marker='.')
plt.axvline(x=1/T_Cu_Si[455], ls='--', color='grey', label='extrinsisch')
plt.axvline(x=1/T_Cu_Si[800], ls='-.', color='grey', label='intrinsisch')
plt.savefig('lnsigma_ohne_fit.png')

plt.figure(8)
plt.xlabel(r'$T^{-1}$ [$K^{-1}$]')
plt.ylabel(r'ln($\sigma$)')
plt.plot(1/T_Cu_Si[430:], np.log(unumpy.nominal_values(el_L[430:])), ls='', marker='.')
plt.plot(1/x, y, ls='--', lw=2, color='r')
plt.plot(1/x_in, y_in, ls='--', lw=2, color='r')
#plt.plot(1/x, y-2.45, ls='--', lw=2, color='r', label=r'linearer Fit für $\ln(\sigma)$')
#plt.plot(1/x, fitted_el_L_Ed)
# plt.errorbar(1/T_Cu_Si[430:], np.log(unumpy.nominal_values(el_L[430:])), yerr=unumpy.std_devs(el_L[430:])/unumpy.nominal_values(el_L[430:]), ls='', marker='+')
plt.savefig('lnsigma_mit_fit.png')

plt.figure(9)
plt.xlabel(r'ln($T$)')
plt.ylabel(r'ln($\mu$)')
plt.plot(np.log(T_Cu_Si), np.log(mu), ls='', marker='.')
#plt.plot(T_Cu_Si, mu, ls='', marker='.')
plt.savefig('beweglichkeitplot.png')

plt.figure(10)
plt.xlabel(r'$T^{-1}$ [$K^{-1}$]')
plt.ylabel(r'ln($\sigma\cdot$ Faktor)')
plt.plot(1/T_Cu_Si[430:], np.log(unumpy.nominal_values(el_L[430:])), color='r', ls='', marker='.', label=r'$\ln\left(\sigma\right)$')
plt.plot(1/T_Cu_Si[430:], np.log(unumpy.nominal_values(el_L[430:])/(T_Cu_Si[430:]**(3/2))), ls='', marker='.', label=r'$\ln\left(\frac{\sigma}{T^{3/2}}\right)$')
plt.plot(1/T_Cu_Si[430:], np.log(unumpy.nominal_values(el_L[430:])/(T_Cu_Si[430:]**(3))), ls='', marker='.', label=r'$\ln\left(\frac{\sigma}{T^3}\right)$')
plt.plot(1/x, y32, ls='--', lw=2, color='r')
plt.plot(1/x, y3, ls='--', lw=2, color='b')
plt.plot(1/x_in, y2_in-3.3, ls='--', lw=2, color='r')
plt.plot(1/x_in, y3_in, ls='--', lw=2, color='b')
plt.legend()
plt.savefig('Silizium_alternativ.png')

"""
Silizium alternativ
"""
data_infit1 = scipy.odr.RealData(T_Cu_Si[430:], np.log(unumpy.nominal_values(el_L[430:])), sy=unumpy.std_devs(el_L)/unumpy.nominal_values(el_L))
in_model = scipy.odr.Model(infit1)
odr_in = scipy.odr.ODR(data_infit1, in_model, beta0=[206, -11])
odr_in_out = odr_in.run()
beta_in = unumpy.uarray(odr_in_out.beta, odr_in_out.sd_beta)
x_in = np.linspace(T_Cu_Si[430], T_Cu_Si[-1], len(T_Cu_Si[430:]))
y_in = in_model.fcn(odr_in_out.beta, x_in)

data2_infit1 = scipy.odr.RealData(T_Cu_Si[430:], np.log(unumpy.nominal_values(el_L[430:])/(T_Cu_Si[430:]**(3/2))), sy=(unumpy.std_devs(el_L)/unumpy.nominal_values(el_L)))
in_model = scipy.odr.Model(infit1)
odr2_in = scipy.odr.ODR(data2_infit1, in_model, beta0=[-60, -21])
odr2_in_out = odr_in.run()
beta2_in = unumpy.uarray(odr2_in_out.beta, odr2_in_out.sd_beta)
x_in = np.linspace(T_Cu_Si[430], T_Cu_Si[-1], len(T_Cu_Si[430:]))
y_in = in_model.fcn(odr_in_out.beta, x_in)
y2_in = in_model.fcn(odr2_in_out.beta, x_in)

data3_infit1 = scipy.odr.RealData(T_Cu_Si[430:], np.log(unumpy.nominal_values(el_L[430:])/(T_Cu_Si[430:]**(3))), sy=unumpy.std_devs(el_L)/unumpy.nominal_values(el_L))
in_model = scipy.odr.Model(infit1)
odr3_in = scipy.odr.ODR(data3_infit1, in_model, beta0=[206, -11])
odr3_in_out = odr3_in.run()
beta3_in = unumpy.uarray(odr3_in_out.beta, odr3_in_out.sd_beta)
x_in = np.linspace(T_Cu_Si[430], T_Cu_Si[-1], len(T_Cu_Si[430:]))
y3_in = in_model.fcn(odr3_in_out.beta, x_in)

E_A_in = beta_in*2*8.617e-5
print(E_A_in)
E_A2_in = beta2_in*2*8.617e-5
print(beta2_in)
print(E_A2_in)
E_A3_in = beta3_in*2*8.617e-5
print(E_A3_in)

def infit2(T, E_A, A):
    return(E_A/T + A)


iopt, icov = curve_fit(infit2, T_Cu_Si[430:], np.log(unumpy.nominal_values(el_L[430:])/(T_Cu_Si[430:]**(3/2))), p0=([-10, 240]))
print('=======')
print(iopt)
print(iopt[0]*2*8.617e-5)
print('=======')

plt.figure(11)
plt.xlabel(r'$T^{-1}$ [$K^{-1}$]')
plt.ylabel(r'ln($\sigma\cdot$ Faktor)')
plt.plot(1/T_Cu_Si[430:], np.log(unumpy.nominal_values(el_L[430:])), color='r', ls='', marker='.', label=r'$\ln\left(\sigma\right)$')
plt.plot(1/T_Cu_Si[430:], np.log(unumpy.nominal_values(el_L[430:])/(T_Cu_Si[430:]**(3/2))), ls='', marker='.', label=r'$\ln\left(\frac{\sigma}{T^{3/2}}\right)$')
plt.plot(1/T_Cu_Si[430:], np.log(unumpy.nominal_values(el_L[430:])/(T_Cu_Si[430:]**(3))), ls='', marker='.', label=r'$\ln\left(\frac{\sigma}{T^3}\right)$')
plt.plot(1/x_in, y_in+0.5, ls='--', lw=2, color='g')
plt.plot(1/x_in, y2_in, ls='--', lw=2, color='r')
plt.plot(1/x_in, infit2(x_in, *iopt))
plt.plot(1/x_in, y3_in-0.7, ls='--', lw=2, color='b')
plt.legend()
plt.savefig('Silizium_mal_anders.png')
plt.show()

plt.figure(12)
plt.xlabel(r'ln($T$)')
plt.ylabel(r'ln($\mu$)')
plt.plot(np.log(T_Cu_Si), np.log(mu), ls='', marker='.')
plt.axvline(x=np.log(T_Cu_Si[1900]), ls='--', color='grey')
plt.savefig('beweglichkeit_alternativ.png')


"""
Vergleich Kupfer Niob
"""
plt.figure(17)
plt.xlabel(r'$T/\Theta_D$')
plt.ylabel(r'$R/R(\Theta_D)$')
plt.plot(T_Cu_Si/T_debye, R_red/R_debye, ls='', marker='.', ms=0.8, label=r'$R_{Cu}/R(\Theta_D)$')
plt.plot(T_Nb[1100:4106]/T_Nb_debye, R_Nb_red[1100:4106]/R_Nb_debye, ls='', marker='.', ms=0.8, label=r'$R_{Nb}/R(\Theta_D)$')
plt.plot(red_T_theo[0:2], red_R_D_theo[0:2], ls='', marker='^', color='r', label=r'Na$_{theo}$')
plt.plot(red_T_theo[2:5], red_R_D_theo[2:5], ls='', marker='8', color='r', label=r'Au$_{theo}$')
plt.plot(red_T_theo[5:9], red_R_D_theo[5:9], ls='', marker='s', color='r', label=r'Cu$_{theo}$')
plt.plot(red_T_theo[9:13], red_R_D_theo[9:13], ls='', marker='P', color='r', label=r'Al$_{theo}$')
plt.plot(red_T_theo[13:16], red_R_D_theo[13:16], ls='', marker='*', color='r', label=r'Ni$_{theo}$')
plt.legend()
plt.savefig('vergleich_mess_theo2.png')


"""
Niob 2
"""
B_i = 0.03

T_c = (T_Nb[4109]+T_Nb[4110])/2
print(T_c)
print(T_Nb[4109])
print(T_Nb[4110])
T_c1 = np.mean(T_Nb[4106:4114])

print(T_c1)
print('===')

T_1 = T_Nb[4106]
T_2 = T_c
print(T_1)

R_Nb_rest = np.mean(R_Nb[4000:4106])
R_Nb_red = R_Nb-R_Nb_rest
R_Nb_spez = R_Nb*1*0.7*1000/15
el_L_Nb = 1/R_Nb_spez
T_min = np.min(T_Nb)
Bc0 = B_i/(1-(T_1/T_2)**2)
print(Bc0)
DBc0 = np.sqrt((0.002/(1-(T_1/T_2)**2))**2 + (2*B_i*T_2**2*T_1*0.2/((T_2**2-T_1**2)**2))**2 + (2*B_i*T_1**2*T_2*0.1/((T_2**2-T_1**2)**2))**2)
print(DBc0)
B_c2 = -Bc0*(1-(T_Nb[4110:4300]/T_2)**2)
print(B_c2[4])
print(T_Nb[4110])
DB_c2 = np.sqrt((DBc0*(1-(T_Nb[4110:4300]/T_2)**2))**2 + (0.1*Bc0*2*T_Nb[4110:4300]**2/T_2**3)**2)
Bc2 = unumpy.uarray(B_c2, DB_c2)
print(DB_c2)
S = np.polyfit(T_Nb[4110:4120], B_c2[0:10], 1)
print(S)
f_S = np.poly1d(S)
print(f_S)

cohlen = np.sqrt(-2.07e-15/(2*np.pi*T_2*S[0]))
Dcohlen = np.sqrt((0.5*np.sqrt(-2.07e-15/(2*np.pi*S[0]))*0.1/(T_2)**(3/2))**2)
print(cohlen, Dcohlen)
cohlen_2 = np.sqrt(-2.07e-15/(2*np.pi*Bc0))
Dcohlen_2 = np.sqrt((0.5*np.sqrt(2.07e-15/(2*np.pi))*DBc0/(-Bc0)**(3/2))**2)
print(cohlen_2, Dcohlen_2)

l_1 = cohlen**2/(39e-9)
dl_1 = 2*cohlen*Dcohlen/39e-9
l_2 = cohlen_2**2/(39e-9)
dl_2 = 2*Dcohlen_2*cohlen_2/39e-9
print(l_1, dl_1, l_2, dl_2)

def Nbgb_fit1(B, x):
    return(1.17*B[0]*x/B[1] - 0.17*B[0])

def Nbgb_fit2(x, a, b):
    return(1.17*a*x/b - 0.17*a)

def linear_fit(B, x):
    return(B[0]*x - B[1])

data_Nbgbfit = scipy.odr.RealData(T_Nb[0:3200], R_Nb_red[0:3200])
Nbgb_model = scipy.odr.Model(Nbgb_fit1)
odr_Nbgb = scipy.odr.ODR(data_Nbgbfit, Nbgb_model, beta0=[1, 270])
odr_Nbgb_out = odr_Nbgb.run()
Nbbeta1 = unumpy.uarray(odr_Nbgb_out.beta, odr_Nbgb_out.sd_beta)
T_Nb_debye = unumpy.nominal_values(Nbbeta1[1])
print(Nbbeta1)

data_linearfit = scipy.odr.RealData(T_Nb[4110:4120], B_c2[0:10], sy=unumpy.std_devs(Bc2[0:10]))
linear_model = scipy.odr.Model(linear_fit)
odr_linear = scipy.odr.ODR(data_linearfit, linear_model, beta0=[1, 270])
odr_linear_out = odr_linear.run()
linbeta1 = unumpy.uarray(odr_linear_out.beta, odr_linear_out.sd_beta)
print(linbeta1)


T_Nb_new = np.linspace(T_Nb[0], T_Nb[3200], len(T_Nb[0:3200]))
fitted_R_Nb_T = Nbgb_model.fcn(odr_Nbgb_out.beta, T_Nb_new)
print(T_Nb_debye)

R_Nb_debye = Nbgb_fit1(odr_Nbgb_out.beta, T_Nb_debye)

plt.figure(113)
plt.xlabel('T [K]')
plt.ylabel(r'R [$\Omega$]')
plt.plot(T_Nb, R_Nb, marker='.', ls='')
plt.plot(T_Nb[2000:2700], R_Nb[2000:2700], marker='.', ls='', color='r')
plt.savefig('Niob_Messung_alt.png')

plt.figure(114)
plt.xlabel('T [K]')
plt.ylabel(r'R [$\Omega$]')
plt.plot(T_Nb[0:5000], R_Nb[0:5000] - R_Nb_rest, marker='.', ls='')
plt.plot(T_Nb[4106:4300], R_Nb[4106:4300] - R_Nb_rest, marker='.', ls='', color='r')

plt.figure(115)
plt.xlabel('T [K]')
plt.ylabel(r'R [$\Omega$]')
plt.plot(T_Nb[4000:7279], R_Nb[4000:7279], marker='.', ls='')
plt.plot(T_Nb[4000:7279], R_Nb[4000:7279], marker='', ls='-', lw=0.5, color='grey')
#plt.plot(T_Nb[4106:4300], R_Nb[4106:4300], marker='+', ls='', color='green')
#plt.plot(T_Nb[4300:7279], R_Nb[4300:7279], marker='x', ls='', color='red',alpha=0.2)
#plt.plot(T_Nb[4400:7000], R_Nb[4400:7000], marker='.', ls='', color='r')
plt.axvline(x=T_2, color='green', alpha=0.8, ls='-.', label=r'$T_c$ im magn. Feld B = 0.03 T')
plt.axvline(x=12.2, color='red', alpha=0.6, ls='-.', label=r'$T_c$ im magn. NullFeld')
plt.axvline(x=T_1, color='orange', alpha=0.4, ls='-.', label=r'Beginn normalleit. Bereich')
plt.legend()
plt.savefig('Niob_T_c_alt.png')

plt.figure(116)
plt.xlabel('T [K]')
plt.ylabel(r'B [T]')
plt.plot(T_Nb[4110:4300], B_c2, ls='', marker='.')
plt.plot(T_Nb[4110:4125], f_S(T_Nb[4110:4125]), ls='--', color='r', lw='2')
plt.savefig('B_crit2_alt.png')
