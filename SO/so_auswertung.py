import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sp
from scipy.optimize import curve_fit
import scipy.odr

from uncertainties import unumpy

import os
import glob

path = 'DATA/'

DataDict = {}

def refl_and_transm(R, T):
    n_R = (1 + np.sqrt(2*R-R**2))/(1-R)
    n_T = (1 + np.sqrt(1-T**2))/T
    return(n_R, n_T)

for filename in glob.glob(os.path.join(path, '*.txt')):
    newname = filename[5:-4]
    if filename[5] == '0':
        DataDict[newname] = np.genfromtxt(filename, skip_header=16, skip_footer=1)
    else:
        DataDict[newname] = np.genfromtxt(filename, skip_header=2, delimiter='\t')

keyslist = list(DataDict.keys())


for key in DataDict.keys():
    if 'farbfilter' in key:
        plt.figure(1)
        plt.xlabel(r'$\lambda$ [nm]')
        plt.ylabel('T [%]')
        if 'Blau' in key:
            bpind, _ = sp.find_peaks(DataDict[key][0:,1], height=50)
            plt.plot(DataDict[key][0:,0], DataDict[key][0:,1], color='b', label=key[10:15])
            plt.axvline(x=DataDict[key][bpind,0], ls='--', color='b', label=r'$\lambda=$ {} nm'.format(np.int(DataDict[key][bpind,0])))
        if 'Rot' in key:
            plt.plot(DataDict[key][0:,0], DataDict[key][0:,1], color='r', label=key[10:15])
            plt.axvline(x=615, color='r', ls='--', label=r'$\lambda=615$ nm')
        if 'Gelb' in key:
            plt.plot(DataDict[key][0:,0], DataDict[key][0:,1], color='yellow', label=key[10:15])
            plt.axvline(x=497, color='yellow', ls='--', label=r'$\lambda=497$ nm')

plt.legend()
plt.savefig('Vergleich_Farbfilter.png')

for key in DataDict.keys():
    if 'glas' in key:
        plt.figure(2)
        plt.xlabel(r'$\lambda$ [nm]')
        plt.ylabel('T bzw. R [%]')
        plt.plot(DataDict[key][0:,0], DataDict[key][0:,1], label=key[5:])
plt.axvline(x=320, color='grey', ls='--', label=r'$\lambda=320$ nm')

plt.legend()
plt.savefig('Vergleich_Reflexion_Transmission_Glas.png')

plt.figure(3)
plt.plot(DataDict[keyslist[-2]][0:,0], DataDict[keyslist[-2]][0:,1])

plt.figure(4)
ffwrpind, _ = sp.find_peaks(DataDict[keyslist[1]][0:,1], height=4000)
plt.xlabel(r'$\lambda$ [nm]')
plt.ylabel('R [%]')
plt.ylim(0,200)
plt.plot(DataDict[keyslist[1]][0:,0], DataDict[keyslist[1]][0:,1])
plt.axvline(x=DataDict[keyslist[1]][ffwrpind,0], ls='--', color='grey', label=r'$\lambda=835$ nm')
plt.legend()
plt.savefig('FFW_Reflexion.png')

plt.figure(5)
ffwtpind, _ = sp.find_peaks(DataDict[keyslist[14]][0:,1], height=15)
plt.xlabel(r'$\lambda$ [nm]')
plt.ylabel('T [%]')
plt.plot(DataDict[keyslist[14]][0:,0], DataDict[keyslist[14]][0:,1])
plt.axvline(x=DataDict[keyslist[14]][ffwtpind,0], ls='--', color='grey', label=r'$\lambda=508$ nm')
plt.legend()
plt.savefig('FFW_Transmission.png')

plt.figure(6)
ptcdatpind, _ = sp.find_peaks(1/DataDict[keyslist[-1]][0:,1])
plt.xlabel(r'$\lambda$ [nm]')
plt.ylabel('T bzw. R [%]')
plt.plot(DataDict[keyslist[-1]][0:,0], DataDict[keyslist[-1]][0:,1])
#plt.plot(DataDict[keyslist[-1]][ptcdatpind,0], DataDict[keyslist[-1]][ptcdatpind,1])
plt.axvline(x=DataDict[keyslist[-1]][ptcdatpind[0],0], ls='--', color='r', label=r'$\lambda=360$ nm')
plt.axvline(x=DataDict[keyslist[-1]][ptcdatpind[1],0], ls='--', color='g', label=r'$\lambda=376$ nm')
plt.axvline(x=DataDict[keyslist[-1]][ptcdatpind[2],0], ls='--', color='b', label=r'$\lambda=478$ nm')
plt.axvline(x=DataDict[keyslist[-1]][ptcdatpind[3],0], ls='--', color='orange', label=r'$\lambda=554$ nm')
plt.legend()
plt.savefig('PTCDA_Transmission.png')

plt.figure(7)
mifupind, _ = sp.find_peaks(DataDict[keyslist[-7]][0:,1], height=30)
mifgpind, _ = sp.find_peaks(DataDict[keyslist[-8]][0:,1], height=30)
mifdgpind, _ = sp.find_peaks(DataDict[keyslist[-11]][0:,1], height=30)
plt.xlabel(r'$\lambda$ [nm]')
plt.xlim(500,600)
plt.ylabel('T [%]')
plt.plot(DataDict[keyslist[-7]][0:,0], DataDict[keyslist[-7]][0:,1], color='r', label='MIF ungeneigt')
plt.plot(DataDict[keyslist[-8]][0:,0], DataDict[keyslist[-8]][0:,1], color='g', label='MIF geneigt')
plt.plot(DataDict[keyslist[-11]][0:,0], DataDict[keyslist[-11]][0:,1], color='b', label='MIF doppelt gekippt')
plt.axvline(x=DataDict[keyslist[-7]][mifupind,0], color='r', ls='--', alpha=0.4, label=r'$\lambda = 550$ nm')
plt.axvline(x=DataDict[keyslist[-8]][mifgpind,0], color='g', ls='--', alpha=0.4, label=r'$\lambda = 542$ nm')
plt.axvline(x=DataDict[keyslist[-11]][mifdgpind,0], color='b', ls='--', alpha=0.4, label=r'$\lambda = 544$ nm')

plt.legend()
plt.savefig('Vergleich_MIF_Kippung.png')

for key in DataDict.keys():
    if key[7] == '1' and key[0] == '0':
        plt.figure(8)
        emokpind, _ = sp.find_peaks(DataDict[key][0:,1], height=1.8e8)
        plt.xlabel(r'$\lambda$ [nm]')
        plt.ylabel(r'Zählrate [$s^{-1}$]')
        plt.plot(DataDict[key][0:,0], DataDict[key][0:,1])
        plt.axvline(x=DataDict[key][emokpind,0], color='grey', ls='--', label=r'$\lambda = 750$ nm')
        plt.legend()
        plt.savefig('PTCDA_Lumineszenz_ohne.png')
    if key[7] == '4' and key[0] == '0':
        plt.figure(9)
        emmkpind, _ = sp.find_peaks(DataDict[key][0:,1], height=22000)
        plt.xlabel(r'$\lambda$ [nm]')
        plt.ylabel(r'Zählrate [$s^{-1}$]')
        plt.plot(DataDict[key][0:,0], DataDict[key][0:,1])
        plt.axvline(x=DataDict[key][emmkpind[0],0], color='grey', ls='--', label=r'$\lambda = 710$ nm')
        plt.axvline(x=DataDict[key][emmkpind[1],0], color='grey', ls='--', label=r'$\lambda = 730$ nm')
        plt.axvline(x=720, color='red', ls='--', label=r'$\lambda = 720$ nm')
        plt.legend()
        plt.savefig('PTCDA_Lumineszenz_mit.png')
    if key[7] == '5' and key[0] == '0':
        plt.figure(10)
        exmkpind, _ = sp.find_peaks(DataDict[key][0:,1], height=8000)
        plt.xlabel(r'$\lambda$ [nm]')
        plt.ylabel(r'Zählrate [$s^{-1}$]')
        plt.plot(DataDict[key][0:,0], DataDict[key][0:,1])
        plt.axvline(x=DataDict[key][exmkpind,0], color='grey', ls='--', label=r'$\lambda =467$ nm')
        plt.legend()
        plt.savefig('PTCDA_Lumineszenz_Exitation')
    if key[7] == '6' and key[0] == '0':
        plt.figure(11)
        plt.xlabel(r'$\lambda$ [nm]')
        plt.ylabel(r'Zählrate [$s^{-1}$]')
        plt.plot(DataDict[key][0:,0], DataDict[key][0:,1])
        plt.axvline(x=725, color='grey', ls='--', label=r'$\lambda = 725$ nm')
        plt.legend()
        plt.savefig('PTCDA_Lumineszenz_Emission')

plt.figure(12)
n_R, n_T = refl_and_transm(DataDict[keyslist[0]][0:,1]/100, DataDict[keyslist[6]][0:,1]/100)
n_R_mean = np.mean(n_R[100:-51])
n_T_mean = np.mean(n_T[100:-51])
n_mean = (n_R_mean + n_T_mean)/2
print(n_mean)
plt.xlabel(r'$\lambda$ [nm]')
plt.xlim(400,700)
plt.ylabel('n')
plt.ylim(1,2.5)
plt.plot(DataDict[keyslist[0]][0:,0], n_R, label=r'n aus Transmission')
plt.plot(DataDict[keyslist[6]][0:,0], n_T, label='n aus Reflexion')
plt.axhline(y=1.59, ls='--', color='grey', label='n=1.59')
plt.legend()
plt.savefig('Glas_Brechungsindex.png')
