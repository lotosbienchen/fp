import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sp
from scipy import stats
from scipy.optimize import curve_fit
import scipy.odr
import math
from  analytic_wfm import peakdetect


from uncertainties import unumpy


class Graph:
    def __init__(self, file, lin, angle):
        self.file = file
        self.lin = lin
        self.angle = angle


#plt.ion()



graphs=[Graph('x_sweep_custom_1', (25,70,140,170), 0),
Graph('y_sweep_custom_0', (40,70,130,160), 90),
Graph('sweep_15_custom_0', (18, 28, 70, 85), 15),
Graph('sweep_30_custom_0', (16,30,62,88), 30),
Graph('sweep_45_custom_0', (20,37,65,80), 45),
Graph('sweep_60_custom_0', (18,35,65,80), 60),
Graph('sweep_75_custom_0', (20, 40, 60, 83), 75),
Graph('y_sweep_com_0', (), 90),
Graph('x_sweep_com_0', (), 0),
Graph('y_sweep_gmr_2', (), 90),
Graph('x-sweep-gmr_2', (), 0)]


for g in graphs:


    data = np.loadtxt('./KellerHilgenfeld/'+g.file+'.ma6', skiprows=8)

    R=data[:,3]
    t=data[:,2]
    x=data[:,4]
    y=data[:,5]

    

    indeces=[i for i,v in enumerate(R) if v > 10000]
    R=np.delete(R,indeces)
    t=np.delete(t,indeces)
    x=np.delete(x,indeces)
    y=np.delete(y,indeces)
    t=t-np.min(t)
    
    R=abs(R)

    B=x*np.cos(g.angle/180*np.pi)+y*np.sin(g.angle/180*np.pi)

    

    if len(g.lin) == 4:
        plt.figure(g.file+'_substract_lin')

        plt.subplot(2, 1, 1)
        plt.plot(t, R, '+b')

        plt.ylabel(r'$resistance\left[\Omega\right]$')
        plt.xlabel(r'$time\left[s\right]$')


        indeces=[i for i,v in enumerate(t) if ((v > g.lin[0] and v < g.lin[1]) or (v > g.lin[2] and v < g.lin[3]))]
        R_lin=np.delete(R,indeces)
        t_lin=np.delete(t,indeces)

        slope, intercept, r_value, p_value, std_err = stats.linregress(t_lin,R_lin)
        line = slope*t+intercept


        plt.plot(t_lin, R_lin, '+r')
        plt.plot(t, line, '-')

        plt.subplot(2, 1, 2)
        R=R-slope*t
        plt.plot(t,R)
        plt.ylabel(r'resistance$\left[\Omega\right]$')
        plt.xlabel('time[s]')
        plt.savefig(g.file+'_substract_lin.pdf')


    plt.figure(g.file)
    #plt.plot(B,R)
    plt.plot(B[:len(B)//2],R[:len(R)//2],'b')
    plt.plot(B[len(B)//2:],R[len(R)//2:],'r')

    plt.ylabel(r'$resistance\left[\Omega\right]$')
    plt.xlabel(r'magnetic flux density $\left[T\right]$')
    plt.title('magnetic field direction '+str(g.angle)+'° off of the x Axis')
    plt.savefig(g.file+'.pdf')
    plt.close('all')


graphs=[Graph('rotation_com_0', (25,70,140,170), 0),
Graph('rotation_custom_0', (100,100,130,160), 90),
Graph('rotation_gmr_0', (18, 28, 70, 85), 15)]


for g in graphs:


    data = np.loadtxt('./KellerHilgenfeld/'+g.file+'.ma6', skiprows=8)

    R=data[:,3]
    t=data[:,2]
    x=data[:,4]
    y=data[:,5]

    

    indeces=[i for i,v in enumerate(R) if v > 10000]
    R=np.delete(R,indeces)
    t=np.delete(t,indeces)
    x=np.delete(x,indeces)
    y=np.delete(y,indeces)
    t=t-np.min(t)
    
    R=abs(R)

    B=x*np.cos(g.angle/180*np.pi)+y*np.sin(g.angle/180*np.pi)

    


    plt.figure(g.file)
    plt.plot(t,R)

    plt.ylabel(r'$resistance\left[\Omega\right]$')
    plt.xlabel('time[s]')

    if g.file=='rotation_custom_0':
        indeces=[i for i,v in enumerate(t) if ((v > g.lin[0] and v < g.lin[1]) or (v > g.lin[2] and v < g.lin[3]))]
        R_lin=np.delete(R,indeces)
        t_lin=np.delete(t,indeces)

        slope, intercept, r_value, p_value, std_err = stats.linregress(t_lin,R_lin)
        line = slope*t+intercept


        plt.plot(t_lin, R_lin, '+r')
        plt.plot(t, line, '-')
        R=R-slope*t
    
    plt.savefig(g.file+'.pdf')

    alpha=t/np.max(t)*360

    plt.figure(g.file+'_angle')
    plt.plot(alpha,R)
    plt.ylabel(r'$resistance\left[\Omega\right]$')
    plt.xlabel('angle of the magnetic field [°]')
    plt.savefig(g.file+'_angle.pdf')

    plt.close('all')


i1=np.array([0,1,2,3,4])
r1=np.array([0,18.61, 39.82, 60.97, 85.98])*3
i2=np.array([-4,-3,-2,-1,1,2,3,4])
r2=np.array([6.27, 4.71, 3.16, 1.61, 1.66, 3.23, 4.8 , 6.38])
r2=np.array([0,1.66, 3.23, 4.8 , 6.38])*3


slope1, intercept, r_value, p_value, std_err = stats.linregress(i1,r1)
line1 = slope1*i1+intercept

slope2, intercept, r_value, p_value, std_err = stats.linregress(i1,r2)
line2 = slope2*i1+intercept

print(slope1/slope2)

plt.figure('')
plt.plot(i1,r1,'+r', label='with yoke')
plt.plot(i1,r2,'+g', label='without yoke')
plt.plot(i1,line1,'r',i1,line2,'g')
plt.ylabel(r'magnetic flux density $\left[mT\right]$')
plt.xlabel('current [A]')
plt.legend()
plt.savefig('perm.pdf')




#input("Press Enter to close...")
plt.close('all')



