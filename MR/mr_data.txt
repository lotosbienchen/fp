Messung Hall-Sonde ohne angelegtes Feld
Schwankung zwischen 190-206mO

mit Metallkern

Stromstärke in A Widerstand in O
1           18.61 +/- 0.1
2           39.82 +/- 0.1
3           60.97 +/- 0.1
4           85.98 +/- 0.1

ohne Metallkern

Stromstärke in A  Widerstand in O
4                 6.27 +/- 0.1
3                 4.71 +/- 0.1
2                 3.16 +/- 0.1
1                 1.61 +/- 0.1
-1                1.66 +/- 0.1
-2                3.23 +/- 0.1
-3                4.8  +/- 0.1
-4                6.38 +/- 0.1


Messung GMR

kommerzielle Probe nve aa004-02e 1726a

eigene Probe
Kontakte 1,8,4,5

5-8 20 kOhm
1-4 70 Ohm
1-8 260 Ohm
4-5 700 kOhm

nach y-sweep noch einmal an der probe rumgespielt
