\babel@toc {ngerman}{}
\contentsline {section}{\numberline {1}Versuchsbeschreibung}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Versuchsziel}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Versuchsaufbau}{3}{subsection.1.2}
\contentsline {section}{\numberline {2}Theoretischer Hintergrund}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}Das Debye-Model}{4}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Das freie Elektronengas}{5}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Die BCS-Theorie}{5}{subsection.2.3}
\contentsline {section}{\numberline {3}Messung}{6}{section.3}
\contentsline {subsection}{\numberline {3.1}Vorbereitung zur Messung}{6}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Messfehler}{6}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Bestimmung der Debye-Temperatur und der Zustandsdichte}{7}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Bestimmung der Sprungtemperatur und Sprungh\IeC {\"o}he}{8}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Vergleich mit der BCS-Theorie}{9}{subsection.3.5}
\contentsline {section}{\numberline {4}Auswertung}{12}{section.4}
