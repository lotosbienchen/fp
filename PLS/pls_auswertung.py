import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sp
from scipy.optimize import curve_fit

data = np.genfromtxt('pls.dat')

#erster Wert war falsch gemessen
U = data[2:,0]
I = data[2:,1]
t = data[2:,2]
T1 = data[2:,3]
T2 = data[2:,4]


def calc_capacity(U, I, t, T2, T1):
    """
    Klassische Kapazitätsberechnung
    """
    return U*I*t/(T2-T1)

def exponential_fit(x, a, b, c):
    """
    Formel für exponentiellen Fit
    """
    return a*np.exp(b*x) + c

def capacity_super_conductor(T, a, D):
    """
    Formel für Elektronenanteil im supraleitenden Zustand
    """
    return a*np.exp(D/T)

C_p = calc_capacity(U, I, t, T2, T1)


# Index für Sprungwert
peakind, _ = sp.find_peaks(C_p)
peakind = np.int(peakind[0])

# Auswertungstemperaturarray
T_mean = (T1 + T2)/2

# Sprungtemperatur
T_c = (T_mean[peakind] + T_mean[peakind + 2])/2

# verworfene Rechnung für Sprunghöhe
dC = (C_p[peakind] - C_p[peakind+2])

print(T_c)
print(C_p[peakind], C_p[peakind + 2], dC)

# Temperaturarray für Fit des linearen Bereichs im normalleitenden Zustand
T_new = np.linspace(0, T_mean[-1], 1000)

# Parameter für linearen Fit siehe oben, dabei ist z[0] beta und z[1] gamma
z = np.polyfit(T_mean[peakind+2:]**2, C_p[peakind+2:]/T_mean[peakind+2:], 1)
print(z)

# Bestimmung der Fitfunktion
f = np.poly1d(z)
# Berechnung gefittete Kapazität
C_new = f(T_new**2)
print(f)

# Array für exponentiellen Fit im supraleitenden Bereich
x = T_mean[0:peakind]
y = C_p[0:peakind]

# Array für exponentiellen Fit im normalleitenden Bereich
x1 = T_mean[peakind + 2:-1]
y1 = C_p[peakind + 2:-1]

# Bestimmung der Fitparameter im ersten Bereich
fitting_parameters, covariance = curve_fit(exponential_fit, x, y)
a, b, c = fitting_parameters
print(fitting_parameters)

# Temparray wird auf Sprungtemperatur erweitert und Fit berechnet
next_x = np.linspace(T_mean[0], T_c, 1000)
next_y = exponential_fit(next_x, a, b, c)

# Bestimmung Parameter im ersten Bereich
fitting_parameters1, covariance = curve_fit(exponential_fit, x1, y1)
d, e, f = fitting_parameters1
print(fitting_parameters)

# Temparray startet mit Sprungtemperatur und Fitberechnung
next_x1 = np.linspace(T_c, T_mean[-1], 1000)
next_y1 = exponential_fit(next_x1, d, e, f)

# Bestimmung Sprunghöhe aus Fits (mit Abzug Phononenteil?)
dC_kl = next_y[-1] - next_y1[0]# - z[0]*T_c**3
#print(dC_kl)

# Sprunghöhe laut BCS-Theorie
dC_bcs = 1.43*z[1]*T_c
#print(dC_bcs)

# Elektronenanteil im normalleitenden Bereich
C_n_el = C_p[peakind+2:-1] - z[0]*T_mean[peakind+2:-1]**3
C1_n_el = C_p - z[0]*T_mean**3

# Elektronenanteil im supraleitenden Bereich
C_s_el = C1_n_el[0:peakind]

# Linearer Fit des normalleitenden Bereiches, neues gamma?
z1 = np.polyfit(T_mean[peakind+2:-1], C_n_el, 1)
#print(z1)
f1 = np.poly1d(z1)
print(f1)
# Elektronenanteil im normalen Bereich
C_n_new = f1(next_x1)

# Arrays für Fit für supralleitenden Teil aus BCS
x3 = x
y3 = C1_n_el[0:peakind]

# Fitparameter für BCS-Teil
fitting_parameters2, covariance = curve_fit(capacity_super_conductor, x3, y3)
k, D0 = fitting_parameters2


# Arrays zur Ausweitung auf T_c
next_x3 = next_x
next_y3 = capacity_super_conductor(next_x3, k, D0)

print(fitting_parameters2)

print(next_y3[-1])
#print(C_s_new[-1])
print(C_n_new[0])
# Sprunghöhe graphisch
print(next_y3[-1] - C_n_new[0])
# Sprunghöhe BCS
#print(1.43*C_n_new[0])
# Verhältsnisüberprüfung, es sollte 2.46 sein
print(next_y3[-1]/C_n_new[0])

# logarithmischer Plot
C_log = np.log(C1_n_el)

T_log = T_c/T_mean[0:peakind]
z2 = np.polyfit(T_log, C_log[0:peakind], 1)
f2 = np.poly1d(z2)
#print(f2)

T_log_new = T_c/np.linspace(T_mean[0],T_c, 1000)
C_log_new = f2(T_log_new)

# Plot zur Sprungtemperatur und Sprungtiefe
#plt.xlim(0,210)
#plt.plot(T_mean, C_p, linestyle='', marker='+', color='black')
#plt.plot(T_mean[peakind], C_p[peakind], linestyle='', marker='x', color='r')
#plt.axvline(x=T_mean[peakind], linestyle='--', color='g')
#plt.axvline(x=T_mean[peakind+2], linestyle='--', color='g')
#plt.plot(next_x, next_y, next_x1, next_y1)
#plt.axhline(y=next_y[-1], linestyle='--', color='black')
#plt.axhline(y=next_y1[0], linestyle='--', color='black')

# Plot zur Ermittlung von gamma und beta
#plt.plot(T_mean**2, C_p/T_mean, linestyle='', marker='+', color='black')
#plt.plot(T_new**2, C_new)

# Plot zum Vergleich mit BCS
#plt.plot(T_mean[0:peakind], C_s_el, linestyle='', marker='x')
plt.plot(T_mean, C1_n_el, linestyle='', marker='x')
plt.plot(next_x3, next_y3)
plt.plot(next_x1, C_n_new)
#plt.plot(next_x, C_s_new)
plt.axvline(x=T_c, linestyle='--', color='r')
plt.axhline(y=next_y3[-1], linestyle='--', color='black')
plt.axhline(y=C_n_new[0], linestyle='--', color='black')

# Plot zum Vergleich mit BCS logarithmisch
#plt.plot(T_c/T_mean, C_log, linestyle='', marker='x')
#plt.plot(T_log_new, C_log_new)

plt.show()
