import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sp
from scipy import stats
from scipy.optimize import curve_fit
import scipy.odr
import math
from  analytic_wfm import peakdetect


from uncertainties import unumpy


class Graph:
    def __init__(self, file, name):
        self.file = file
        self.name = name

def theta( alpha ):
    return math.degrees(np.arccos(np.cos(math.radians(alpha))/2-1/2))

plt.ion()




graphs=[Graph('./07.12.2018/Molekuel/Kugelresonator_0-1000.dat','Kugelresonator'),
Graph('./07.12.2018/Molekuel/molekül_5mm_0-1000.dat','5mm Molekülblende'),
Graph('./07.12.2018/Molekuel/molekül_10mm_0-1000.dat','10mm Molekülblende'),
Graph('./07.12.2018/Molekuel/molekül_15mm_0-1000.dat','15mm Molekülblende'),
Graph('./07.12.2018/Molekuel/molekül_20mm_0-1000.dat','20mm Molekülblende')]


f = plt.figure()

for g in graphs:


    data = np.loadtxt(g.file)

    x=data[:,0]
    y=data[:,1]

  


    plt.plot(x, y, linestyle='-', label=g.name)
    
    
    




plt.ylabel('Intensität')
plt.xlabel('Frequenz [Hz]')
plt.legend()
plt.savefig('molekuel_spektrum.pdf')


peaks=np.array([0,210,320,400,460])
iris=np.array([0,5,10,15,20])

f = plt.figure()
plt.plot(iris, peaks, '+')


slope, intercept, r_value, p_value, std_err = stats.linregress(iris,peaks)
line = slope*iris+intercept

#plt.plot(iris, line, linestyle='-')

plt.ylabel('Peakposition [Hz]')
plt.xlabel('Irisblende [mm]')

plt.savefig('resonanz_molekuel.pdf')




graphs=[Graph('./07.12.2018/Molekuel/molekül_5mm_peak2300.dat','5mm Molekülblende'),
Graph('./07.12.2018/Molekuel/molekül_10mm_peak2300.dat','10mm Molekülblende'),
Graph('./07.12.2018/Molekuel/molekül_15mm_peak2300.dat','15mm Molekülblende'),
Graph('./07.12.2018/Molekuel/molekül_20mm_peak2300.dat','20mm Molekülblende')]


f = plt.figure()

for g in graphs:


    data = np.loadtxt(g.file)

    x=data[:,0]
    y=data[:,1]

    indeces=[i for i,v in enumerate(x) if v < 2200]
    x=np.delete(x,indeces)
    y=np.delete(y,indeces)

  


    plt.plot(x, y, linestyle='-', label=g.name)

plt.ylabel('Intensität')
plt.xlabel('Frequenz [Hz]')
plt.legend()
plt.savefig('molekuel_2300.pdf')


data = np.loadtxt('./07.12.2018/Molekuel/molekül_20mm_peak2300_HR.dat')

x=data[:,0]
y=data[:,1]

indeces=[i for i,v in enumerate(x) if v < 2200]
x=np.delete(x,indeces)
y=np.delete(y,indeces)

f = plt.figure()
plt.plot(x, y, linestyle='-')
plt.ylabel('Intensität')
plt.xlabel('Frequenz [Hz]')
plt.savefig('molekuel_2300_HR.pdf')


input("Press Enter to close...")
plt.close('all')