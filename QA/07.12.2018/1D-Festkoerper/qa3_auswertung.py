import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sp
from scipy.optimize import curve_fit
import scipy.odr
from uncertainties import unumpy

def calc_k(n, L):
    return n*np.pi/L

L1 = 0.4
L2 = 0.6

n_t = np.array([1,2,3,4,5,6,7,7,6,5,4,3,2,1,0,0,1,2,3,4,5,6,7,7,6,5,4,3,2,1,0])
n_t2 = np.array([1,2,3,4,5,6,7,7,6,5,4,3,2,1,0,1,2,3,4,5,6,6,5,4,3,2,1])
n_t3 = np.array([1,2,3,4,5,6,7,8,9,10,11,10,9,8,7,6,5,4,3,2,1,2,3,4,5,6,7,8,9,10,11,9,8,7,6,5,4,3,2])
n_t4 = np.array([1,2,3,4,5,6,7,8,9,10,11,11,10,9,8,7,6,5,4,3,2,1,0,1,2,3,4,5,6,7,8,9,10,10,9,8,7,6,5,4,3,2,1])
n_t5 = np.array([1,2,3,4,5,6,7,8,9,10,11,11,10,9,8,7,6,5,4,3,2,1,0,1,2,3,4,5,6,7,8,9,10,11,10,9,8,7,6,5,4,3,2,1])
n_tb = np.array([1,2,3,4,5,6,7,7,6,5,4,3,2,1,0,0,1,2,3,4,5,6,7,7,6,5,4,3,2,1,0,0,1,2,3,4,5,6,7,7,6,5,4,3,2,1,0])

x_gap1 = np.linspace(-calc_k(31, L1), calc_k(31, L1), 5000)
x_gap2 = np.linspace(-calc_k(47, L2), calc_k(47, L2), 5000)


L1data1 = np.genfromtxt('Elementarzelle_5cm/Uebersichtsspektrum_8_Zellen_0-12000_ohne_Ringe.dat')
L1x1 = L1data1[0:,0]
L1y1 = L1data1[0:,1]

L1peakind_1, _ = sp.find_peaks(L1y1, prominence=0.6)
L1n1 = np.linspace(1, len(L1peakind_1), len(L1peakind_1))

L1data2 = np.genfromtxt('Elementarzelle_5cm/Uebersichtsspektrum_8_Zellen_0-12000_Ringe_10mm.dat')
L1x2 = L1data2[0:,0]
L1y2 = L1data2[0:,1]

L1peakind_2, _ = sp.find_peaks(L1y2, height=1.6)
L1peakind_2 = L1peakind_2[4:]
L1n2 = np.linspace(1, len(L1peakind_2), len(L1peakind_2))
for i in range(15,21):
    L1n2[i] = L1n2[i] + 1
for i in range(21,27):
    L1n2[i] = L1n2[i] + 3
#print(L1n2)

L1data3 = np.genfromtxt('Elementarzelle_5cm/Uebersichtsspektrum_8_Zellen_0-12000_Ringe_13mm.dat')
L1x3 = L1data3[0:,0]
L1y3 = L1data3[0:,1]

L1peakind_3, _ = sp.find_peaks(L1y3, height=0.5, prominence=0.03)
L1peakind_3 = L1peakind_3[3:]
index = [2,4]
L1peakind_3 = np.delete(L1peakind_3, index)
L1peakind_3 = np.concatenate([L1peakind_3[0:22], [757, 990], L1peakind_3[23:], [1073]])
L1n3 = np.linspace(1, len(L1peakind_3), len(L1peakind_3))
#print(L1n3)

L1data4 = np.genfromtxt('Elementarzelle_5cm/Uebersichtsspektrum_8_Zellen_0-12000_Ringe_16mm.dat')
L1x4 = L1data4[0:,0]
L1y4 = L1data4[0:,1]

L1peakind_4, _ = sp.find_peaks(L1y4, height=0.5, prominence=0.05)
L1peakind_4 = L1peakind_4[4:]
index = [3, 4, 5, 7, 10]
L1peakind_4 = np.concatenate([[35], L1peakind_4])
L1peakind_4 = np.delete(L1peakind_4, index)
L1n4 = np.linspace(1, len(L1peakind_4), len(L1peakind_4))

#plt.plot(L1x2, L1y2)
#plt.plot(L1x2[L1peakind_2], L1y2[L1peakind_2], ls='', marker='.')
#plt.axvline(x=368, color='r', ls='--')

plt.figure(2)
plt.xlabel(r'Wellenzahl $k$ [m$^{-1}$]')
plt.ylabel(r'Frequenz $f$ [Hz]')
plt.plot(-calc_k(L1n2, L1), L1x2[L1peakind_2], marker='.', ls='', color='b', label='d = 10 mm')
plt.plot(-calc_k(L1n3, L1), L1x3[L1peakind_3], marker='.', ls='', color='r', label='d = 13 mm')
plt.plot(-calc_k(L1n4, L1), L1x4[L1peakind_4], marker='.', ls='', color='g', label='d = 16 mm')
plt.plot(calc_k(L1n2, L1), L1x2[L1peakind_2], marker='.', ls='', color='b')
plt.plot(calc_k(L1n3, L1), L1x3[L1peakind_3], marker='.', ls='', color='r')
plt.plot(calc_k(L1n4, L1), L1x4[L1peakind_4], marker='.', ls='', color='g')
plt.plot(calc_k(n_t2, L1), L1x2[L1peakind_2], -calc_k(n_t2, L1),L1x2[L1peakind_2], marker='.', ls='', color='b')
plt.plot(calc_k(n_t, L1), L1x3[L1peakind_3], -calc_k(n_t, L1),L1x3[L1peakind_3], marker='.', ls='', color='r')
plt.plot(calc_k(n_t, L1), L1x4[L1peakind_4], -calc_k(n_t, L1),L1x4[L1peakind_4], marker='.', ls='', color='g')
plt.axvline(x=calc_k(0, L1), ls='--', color='grey', lw=0.5, alpha=0.4)
plt.axvline(x=calc_k(7, L1), ls='-', color='grey', lw=0.5)
plt.axvline(x=-calc_k(7, L1), ls='-', color='grey', lw=0.5)
plt.axvline(x=calc_k(15, L1), ls='--', color='grey', lw=0.5, alpha=0.4)
plt.axvline(x=-calc_k(15, L1), ls='--', color='grey', lw=0.5, alpha=0.4)
plt.axvline(x=calc_k(23, L1), ls='-', color='grey', lw=0.5)
plt.axvline(x=-calc_k(23, L1), ls='-', color='grey', lw=0.5)
plt.axvline(x=calc_k(31, L1), ls='--', color='grey', lw=0.5, alpha=0.4)
plt.axvline(x=-calc_k(31, L1), ls='--', color='grey', lw=0.5, alpha=0.4)
plt.fill_between(x_gap1, L1x2[L1peakind_2[6]]*np.ones(len(x_gap1)), L1x2[L1peakind_2[7]]*np.ones(len(x_gap1)), color='grey', alpha=0.1)
plt.fill_between(x_gap1, L1x3[L1peakind_3[6]]*np.ones(len(x_gap1)), L1x3[L1peakind_3[7]]*np.ones(len(x_gap1)), color='grey', alpha=0.2)
plt.fill_between(x_gap1, L1x4[L1peakind_4[6]]*np.ones(len(x_gap1)), L1x4[L1peakind_4[7]]*np.ones(len(x_gap1)), color='grey', alpha=0.4)
plt.fill_between(x_gap1, L1x2[L1peakind_2[14]]*np.ones(len(x_gap1)), L1x2[L1peakind_2[15]]*np.ones(len(x_gap1)), color='grey', alpha=0.1)
plt.fill_between(x_gap1, L1x3[L1peakind_3[14]]*np.ones(len(x_gap1)), L1x3[L1peakind_3[15]]*np.ones(len(x_gap1)), color='grey', alpha=0.2)
plt.fill_between(x_gap1, L1x4[L1peakind_4[14]]*np.ones(len(x_gap1)), L1x4[L1peakind_4[15]]*np.ones(len(x_gap1)), color='grey', alpha=0.4)
#plt.fill_between(x_gap1, L2x2[L2peakind_2[22]]*np.ones(len(x_gap1)), L2x2[L2peakind_2[23]]*np.ones(len(x_gap1)), color='grey', alpha=0.4)
plt.fill_between(x_gap1, L1x3[L1peakind_3[22]]*np.ones(len(x_gap1)), L1x3[L1peakind_3[23]]*np.ones(len(x_gap1)), color='grey', alpha=0.2)
plt.fill_between(x_gap1, L1x4[L1peakind_4[22]]*np.ones(len(x_gap1)), L1x4[L1peakind_4[23]]*np.ones(len(x_gap1)), color='grey', alpha=0.4)
plt.legend()
plt.savefig('../../gap_L400.png')

L2data1 = np.genfromtxt('Elementarzelle_5cm/Uebersichtsspektrum_12_Zellen_0-12000_ohne_Ringe.dat')
L2x1 = L2data1[0:,0]
L2y1 = L2data1[0:,1]

L2peakind_1, _ = sp.find_peaks(L2y1, prominence=0.6)
L2n1 = np.linspace(1, len(L2peakind_1), len(L2peakind_1))

L2data2 = np.genfromtxt('Elementarzelle_5cm/Uebersichtsspektrum_12_Zellen_0-12000_Ringe_10mm.dat')
L2x2 = L2data2[0:,0]
L2y2 = L2data2[0:,1]

L2peakind_2, _ = sp.find_peaks(L2y2, height=0.8, prominence=0.05)
L2peakind_2 = L2peakind_2[1:]
index = [1,3,4,6]
L2peakind_2 = np.delete(L2peakind_2, index)
L2peakind_2 = np.concatenate([L2peakind_2[0:10], [165], L2peakind_2[10:20], [669], L2peakind_2[20:28], [726, 1007], L2peakind_2[28:], [1047]])

L2n2 = np.linspace(1, len(L2peakind_2), len(L2peakind_2))
for i in range(11,21):
    L2n2[i] = L2n2[i] + 1
for i in range(21,31):
    L2n2[i] = L2n2[i] + 4
for i in range(31,39):
    L2n2[i] = L2n2[i] + 6

L2data3 = np.genfromtxt('Elementarzelle_5cm/Uebersichtsspektrum_12_Zellen_0-12000_Ringe_13mm.dat')
L2x3 = L2data3[0:,0]
L2y3 = L2data3[0:,1]

L2peakind_3, _ = sp.find_peaks(L2y3, height=0.5, prominence=0.03)
L2peakind_3 = np.concatenate([L2peakind_3[0:22], [462], L2peakind_3[22:31], [755, 992], L2peakind_3[31:], [1073]])
L2n3 = np.linspace(1, len(L2peakind_3), len(L2peakind_3))
for i in range(23,33):
    L2n3[i] = L2n3[i] + 2
for i in range(33,43):
    L2n3[i] = L2n3[i] + 3

L2data4 = np.genfromtxt('Elementarzelle_5cm/Uebersichtsspektrum_12_Zellen_0-12000_Ringe_16mm.dat')
L2x4 = L2data4[0:,0]
L2y4 = L2data4[0:,1]

L2peakind_4, _ = sp.find_peaks(L2y4, height=0.5, prominence=0.05)
L2peakind_4 = L2peakind_4[2:]
index = [1, 2]
L2peakind_4 = np.delete(L2peakind_4, index)
L2peakind_4 = np.concatenate([L2peakind_4[0:23], [644], L2peakind_4[23:]])
L2n4 = np.linspace(1, len(L2peakind_4), len(L2peakind_4))
for i in range(23,34):
    L2n4[i] = L2n4[i] + 1
for i in range(34,44):
    L2n4[i] = L2n4[i] + 2

plt.figure(3)

plt.plot(L2x4, L2y4)
plt.plot(L2x4[L2peakind_4], L2y4[L2peakind_4], ls='', marker='.')
plt.figure(4)
plt.xlabel(r'Wellenzahl $k$ [m$^{-1}$]')
plt.ylabel(r'Frequenz $f$ [Hz]')
# #plt.plot(calc_k(L2n1, L2), L2x1[L2peakind_1], -calc_k(L2n1, L2), L2x1[L2peakind_1], marker='.', ls='')
plt.plot(calc_k(L2n2, L2), L2x2[L2peakind_2], marker='.', ls='', color='b', label='d = 10 mm')
plt.plot(calc_k(L2n3, L2), L2x3[L2peakind_3], marker='.', ls='', color='r', label='d = 13 mm')
plt.plot(calc_k(L2n4, L2), L2x4[L2peakind_4], marker='.', ls='', color='g', label='d = 16 mm')
plt.plot(-calc_k(L2n2, L2), L2x2[L2peakind_2], marker='.', ls='', color='b')
plt.plot(-calc_k(L2n3, L2), L2x3[L2peakind_3], marker='.', ls='', color='r')
plt.plot(-calc_k(L2n4, L2), L2x4[L2peakind_4], marker='.', ls='', color='g')
plt.plot(calc_k(n_t3, L2), L2x2[L2peakind_2], -calc_k(n_t3, L2),L2x2[L2peakind_2], marker='.', ls='', color='b')
plt.plot(calc_k(n_t4, L2), L2x3[L2peakind_3], -calc_k(n_t4, L2),L2x3[L2peakind_3], marker='.', ls='', color='r')
plt.plot(calc_k(n_t5, L2), L2x4[L2peakind_4], -calc_k(n_t5, L2),L2x4[L2peakind_4], marker='.', ls='', color='g')
# #plt.plot(x_gap, L2x2[L2peakind_2[6]]*np.ones(len(x_gap)))
plt.axvline(x=calc_k(0, L2), ls='--', color='grey', lw=0.5, alpha=0.4)
plt.axvline(x=calc_k(11, L2), ls='-', color='grey', lw=0.5)
plt.axvline(x=-calc_k(11, L2), ls='-', color='grey', lw=0.5)
plt.axvline(x=calc_k(23, L2), ls='--', color='grey', lw=0.5, alpha=0.4)
plt.axvline(x=-calc_k(23, L2), ls='--', color='grey', lw=0.5, alpha=0.4)
plt.axvline(x=calc_k(35, L2), ls='-', color='grey', lw=0.5)
plt.axvline(x=-calc_k(35, L2), ls='-', color='grey', lw=0.5)
plt.axvline(x=calc_k(47, L2), ls='--', color='grey', lw=0.5, alpha=0.4)
plt.axvline(x=-calc_k(47, L2), ls='--', color='grey', lw=0.5, alpha=0.4)
plt.fill_between(x_gap2, L2x2[L2peakind_2[10]]*np.ones(len(x_gap2)), L2x2[L2peakind_2[11]]*np.ones(len(x_gap2)), color='grey', alpha=0.1)
plt.fill_between(x_gap2, L2x3[L2peakind_3[10]]*np.ones(len(x_gap2)), L2x3[L2peakind_3[11]]*np.ones(len(x_gap2)), color='grey', alpha=0.2)
plt.fill_between(x_gap2, L2x4[L2peakind_4[10]]*np.ones(len(x_gap2)), L2x4[L2peakind_4[11]]*np.ones(len(x_gap2)), color='grey', alpha=0.4)
#plt.fill_between(x_gap2, L2x2[L2peakind_2[22]]*np.ones(len(x_gap2)), L2x2[L2peakind_2[23]]*np.ones(len(x_gap2)), color='grey', alpha=0.1)
plt.fill_between(x_gap2, L2x3[L2peakind_3[22]]*np.ones(len(x_gap2)), L2x3[L2peakind_3[23]]*np.ones(len(x_gap2)), color='grey', alpha=0.2)
plt.fill_between(x_gap2, L2x4[L2peakind_4[22]]*np.ones(len(x_gap2)), L2x4[L2peakind_4[23]]*np.ones(len(x_gap2)), color='grey', alpha=0.4)
#plt.fill_between(x_gap2, L2x2[L2peakind_2[22]]*np.ones(len(x_gap2)), L2x2[L2peakind_2[23]]*np.ones(len(x_gap2)), color='grey', alpha=0.4)
#plt.fill_between(x_gap2, L2x3[L2peakind_3[33]]*np.ones(len(x_gap2)), L2x3[L2peakind_3[34]]*np.ones(len(x_gap2)), color='grey', alpha=0.2)
plt.fill_between(x_gap2, L2x4[L2peakind_4[33]]*np.ones(len(x_gap2)), L2x4[L2peakind_4[34]]*np.ones(len(x_gap2)), color='grey', alpha=0.4)
plt.legend()
plt.savefig('../../gap_L600.png')



plt.figure(5)
plt.xlabel(r'Wellenzahl $k$ [m$^{-1}$]')
plt.ylabel(r'Frequenz $f$ [Hz]')
plt.plot(calc_k(L1n1, L1), L1x1[L1peakind_1], ls='', marker='+', label='L = 400 mm')
plt.plot(calc_k(L2n1, L2), L2x1[L2peakind_1], ls='', marker='x', label='L = 600 mm')
plt.legend()
plt.savefig('../../compare_L.png')

"""
So, das war der erste Foo. Jetzt kommt das mit der Zustandsdichte. Es kann nur
schlimmer werden.
"""

def dos(f1, f2):
    return 1/(f2-f1)

j = 0
f = L1x4[L1peakind_4]
DOS = []
df = []
kDOS = []
for i in range(4):
    for i in range(6):
        DOS.append(dos(f[i+j*8], f[i+j*8+1]))
        df.append(f[i+j*8+1] - f[i+j*8])
    j = j + 1

mean_dos = np.mean(DOS)
std_dos = np.std(DOS)
print(mean_dos)
err_dos = std_dos/np.sqrt(len(DOS))
print(err_dos)

"""
Zweiter Versuch Zustandsdichte
"""
def kdos(k1, k2):
    return 1/(2.97e8/(2*np.pi)*(k2 - k1))
k = calc_k(L1n4, L1)
m = 0
DOS2 = []
dk = []
df2 = []
for i in range(1,len(k)):
    kDOS.append(kdos(k[i-1], k[i]))
    DOS2.append(dos(f[i-1], f[i]))
    dk.append(k[i]-k[i-1])
    df2.append(f[i] - f[i-1])


print(k)
print(len(f))
print(DOS)
print(kDOS)
plt.figure(128)
plt.xlabel(r'Wellenzahl $k$ [m$^{-1}$]')
plt.ylabel(r'Zustandsdichte $\rho$ [s]')
plt.plot(k[1:], DOS2, ls='', marker='.')
plt.plot(k[1:], DOS2, ls='--', marker='', lw=0.2, color='black')
plt.savefig('../../Zustandsdichte.png')
plt.show()

"""
Spielt den Song nochmal für eine Elementarzelle a 7.5cm
"""

data_b = np.genfromtxt('Elementarzelle_7,5cm/Uebersichtsspektrum_8_Zellen_0-12000_Ringe16mm.dat')
L2x_b = data_b[0:,0]
L2y_b = data_b[0:,1]

L2pi_b, _ = sp.find_peaks(L2y_b, height=0.5, prominence=0.05)
L2pi_b = L2pi_b[3:]
index = [1, 3, 5, 6]
L2pi_b = np.delete(L2pi_b, index)
L2pi_b = np.concatenate([L2pi_b[0:38], [960, 1090], L2pi_b[38:], [1175]])
L2nb = np.linspace(1, len(L2x_b[L2pi_b]), len(L2x_b[L2pi_b]))

print(L2pi_b)
print(L2nb)

plt.figure(6)
plt.plot(L2x_b, L2y_b)
plt.plot(L2x_b[L2pi_b], L2y_b[L2pi_b], ls='', marker='.')
plt.figure(7)
plt.xlabel(r'Wellenzahl $k$ [m$^{-1}$]')
plt.ylabel(r'Frequenz $f$ [Hz]')
plt.plot(-calc_k(L1n4, L1), L1x4[L1peakind_4], marker='.', ls='', color='g', label='EZ 50 mm')
plt.plot(-calc_k(L2nb, L2), L2x_b[L2pi_b], marker='.', ls='', color='b', label='EZ 75 mm')
plt.plot(calc_k(L1n4, L1), L1x4[L1peakind_4], marker='.', ls='', color='g')
plt.plot(calc_k(L2nb, L2), L2x_b[L2pi_b], marker='.', ls='', color='b')
plt.plot(calc_k(n_t, L1), L1x4[L1peakind_4], -calc_k(n_t, L1),L1x4[L1peakind_4], marker='.', ls='', color='g')
plt.plot(calc_k(n_tb, L2), L2x_b[L2pi_b], -calc_k(n_tb, L2), L2x_b[L2pi_b], marker='.', ls='', color='b')
plt.axvline(x=calc_k(0, L1), ls='--', color='grey', lw=0.5, alpha=0.4)
plt.axvline(x=calc_k(7, L1), ls='-', color='grey', lw=0.5)
plt.axvline(x=-calc_k(7, L1), ls='-', color='grey', lw=0.5)
plt.axvline(x=calc_k(15, L1), ls='--', color='grey', lw=0.5, alpha=0.4)
plt.axvline(x=-calc_k(15, L1), ls='--', color='grey', lw=0.5, alpha=0.4)
plt.axvline(x=calc_k(23, L1), ls='-', color='grey', lw=0.5)
plt.axvline(x=-calc_k(23, L1), ls='-', color='grey', lw=0.5)
plt.axvline(x=calc_k(31, L1), ls='--', color='grey', lw=0.5, alpha=0.4)
plt.axvline(x=-calc_k(31, L1), ls='--', color='grey', lw=0.5, alpha=0.4)
plt.axvline(x=calc_k(0, L2), ls='--', color='red', lw=0.5, alpha=0.3)
plt.axvline(x=calc_k(7, L2), ls='-', color='red', lw=0.5)
plt.axvline(x=-calc_k(7, L2), ls='-', color='red', lw=0.5)
plt.axvline(x=calc_k(15, L2), ls='--', color='red', lw=0.5, alpha=0.3)
plt.axvline(x=-calc_k(15, L2), ls='--', color='red', lw=0.5, alpha=0.3)
plt.axvline(x=calc_k(23, L2), ls='-', color='red', lw=0.5)
plt.axvline(x=-calc_k(23, L2), ls='-', color='red', lw=0.5)
plt.axvline(x=calc_k(31, L2), ls='--', color='red', lw=0.5, alpha=0.3)
plt.axvline(x=-calc_k(31, L2), ls='--', color='red', lw=0.5, alpha=0.3)
plt.axvline(x=calc_k(39, L2), ls='-', color='red', lw=0.5)
plt.axvline(x=-calc_k(39, L2), ls='-', color='red', lw=0.5)
plt.axvline(x=calc_k(47, L2), ls='--', color='red', lw=0.5, alpha=0.3)
plt.axvline(x=-calc_k(47, L2), ls='--', color='red', lw=0.5, alpha=0.3)
plt.fill_between(x_gap1, L1x4[L1peakind_4[6]]*np.ones(len(x_gap1)), L1x4[L1peakind_4[7]]*np.ones(len(x_gap1)), color='grey', alpha=0.4)
plt.fill_between(x_gap1, L2x_b[L2pi_b[6]]*np.ones(len(x_gap1)), L2x_b[L2pi_b[7]]*np.ones(len(x_gap1)), color='b', alpha=0.2)
plt.fill_between(x_gap1, L2x_b[L2pi_b[14]]*np.ones(len(x_gap1)), L2x_b[L2pi_b[15]]*np.ones(len(x_gap1)), color='b', alpha=0.2)
plt.fill_between(x_gap1, L1x4[L1peakind_4[14]]*np.ones(len(x_gap1)), L1x4[L1peakind_4[15]]*np.ones(len(x_gap1)), color='grey', alpha=0.4)
#plt.fill_between(x_gap1, L2x2[L2peakind_2[22]]*np.ones(len(x_gap1)), L2x2[L2peakind_2[23]]*np.ones(len(x_gap1)), color='grey', alpha=0.4)
plt.fill_between(x_gap1, L2x_b[L2pi_b[22]]*np.ones(len(x_gap1)), L2x_b[L2pi_b[23]]*np.ones(len(x_gap1)), color='b', alpha=0.2)
plt.fill_between(x_gap1, L1x4[L1peakind_4[22]]*np.ones(len(x_gap1)), L1x4[L1peakind_4[23]]*np.ones(len(x_gap1)), color='grey', alpha=0.4)
plt.fill_between(x_gap1, L2x_b[L2pi_b[30]]*np.ones(len(x_gap1)), L2x_b[L2pi_b[31]]*np.ones(len(x_gap1)), color='b', alpha=0.2)
plt.fill_between(x_gap1, L2x_b[L2pi_b[38]]*np.ones(len(x_gap1)), L2x_b[L2pi_b[39]]*np.ones(len(x_gap1)), color='b', alpha=0.2)
plt.legend()
plt.savefig('../../gap_L1_L2_comp.png')



plt.show()
