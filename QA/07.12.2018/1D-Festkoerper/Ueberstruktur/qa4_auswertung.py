import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sp
from scipy.optimize import curve_fit
import scipy.odr

from uncertainties import unumpy

def calc_k(n, L):
    return n*np.pi/L

L1 = 0.4
L2 = 0.6
L3 = 0.625

n_t1 = np.array([1,2,3,4,5,6,7,8,9,10,11,11,10,9,8,7,6,5,4,3,2,1,0,1,2,3,4,5,6,7,8,9,10,11,9,8,7,6,5,4,3,2,1,])
n_t2 = np.array([1,2,3,4,5,6,7,8,9,10,11,11,10,9,8,7,6,5,4,3,2,1,0,0,1,2,3,4,5,6,7,8,9,10,11,10,9,8,7,6,4,3,2,1,0])
n_t2_ = np.array([1,2,3,4,5,5,4,3,2,1,0,0,1,2,3,4,5,5,4,3,2,1,0,0,1,2,3,4,5,5,4,3,2,1,0,1,2,3,4,5,4,3,2,1,0])
n_t3 = np.array([1,2,3,4,5,6,7,8,9,4,3,2,1,0,0,1,2,3,4,4,3,2,1,0,0,1,2,3,4,5,6,7,8,9,4,3,2,1,0,0,1,2,3,4,4,3,2,1,0])
n_t3_ = np.array([1,2,3,4,5,6,7,8,9,9,8,7,6,5,4,3,2,1,0,0,1,2,3,4,5,6,7,8,9,9,8,7,6,5,4,3,2,1,0,0,1,2,3,4,5,6,7,8,9])

x_gap1 = np.linspace(-calc_k(31, L1), calc_k(31, L1), 5000)
x_gap2 = np.linspace(-calc_k(47, L2), calc_k(47, L2), 5000)
x_gap3 = np.linspace(-calc_k(49, L3), calc_k(49, L3), 5000)

data1 = np.genfromtxt('../Elementarzelle_5cm/Uebersichtsspektrum_12_Zellen_0-12000_Ringe_13mm.dat')
x1 = data1[0:,0]
y1 = data1[0:,1]
peakind_1 , _ = sp.find_peaks(y1, height=0.8, prominence=0.05)
peakind_1 = np.concatenate([peakind_1[0:22], [462], peakind_1[22:31], [755, 992], peakind_1[31:], [1072]])
n1 = np.linspace(1, len(peakind_1), len(peakind_1))
for i in range(23,33):
    n1[i] = n1[i] + 1
for i in range(33,43):
    n1[i] = n1[i] + 3

data2 = np.genfromtxt('Uebersichtsspektrum_12_Zellen_0-12000_Ringe_gemischt_13u16mm.dat')
x2 = data2[0:,0]
y2 = data2[0:,1]
peakind_2 , _ = sp.find_peaks(y2, height=0.3, prominence=0.03)
peakind_2 = peakind_2[3:]
peakind_2 = np.concatenate([peakind_2[0:22], [496, 668], peakind_2[22:27], [720], peakind_2[27:31], [792], peakind_2[31:36], [1064], peakind_2[36:]])
n2 = np.linspace(1, len(peakind_2), len(peakind_2))

for i in range(35,40):
    n2[i] = n2[i] + 1
for i in range(40,45):
    n2[i] = n2[i] + 2

# plt.plot(x1,y1)
# plt.plot(x1[peakind_1], y1[peakind_1], ls='', marker='.')
plt.figure(2)
plt.xlabel(r'Wellenzahl $k$ [m$^{-1}$]')
plt.ylabel(r'Frequenz $f$ [Hz]')
plt.plot(calc_k(n1, L2), x1[peakind_1], ls='', marker='.', color='b', label='ohne Überstruktur')
plt.plot(calc_k(n2, L2), x2[peakind_2], ls='', marker='.', color='r', label='mit Überstruktur')
plt.plot(-calc_k(n1, L2), x1[peakind_1], ls='', marker='.', color='b')
plt.plot(-calc_k(n2, L2), x2[peakind_2], ls='', marker='.', color='r')
plt.plot(calc_k(n_t1, L2), x1[peakind_1], -calc_k(n_t1, L2),x1[peakind_1], marker='.', ls='', color='b')
plt.plot(calc_k(n_t2_, L2), x2[peakind_2], -calc_k(n_t2_, L2),x2[peakind_2], marker='.', ls='', color='r')
plt.axvline(x=calc_k(0, L2), ls='--', color='grey', lw=0.5, alpha=0.4)
plt.axvline(x=calc_k(5, L2), ls='-', color='grey', lw=0.5)
plt.axvline(x=-calc_k(5, L2), ls='-', color='grey', lw=0.5)
plt.axvline(x=calc_k(11, L2), ls='--', color='grey', lw=0.5, alpha=0.4)
plt.axvline(x=-calc_k(11, L2), ls='--', color='grey', lw=0.5, alpha=0.4)
plt.axvline(x=calc_k(17, L2), ls='-', color='grey', lw=0.5)
plt.axvline(x=-calc_k(17, L2), ls='-', color='grey', lw=0.5)
plt.axvline(x=calc_k(23, L2), ls='--', color='grey', lw=0.5, alpha=0.4)
plt.axvline(x=-calc_k(23, L2), ls='--', color='grey', lw=0.5, alpha=0.4)
plt.axvline(x=calc_k(29, L2), ls='-', color='grey', lw=0.5)
plt.axvline(x=-calc_k(29, L2), ls='-', color='grey', lw=0.5)
plt.axvline(x=-calc_k(35, L2), ls='--', color='grey', lw=0.5, alpha=0.4)
plt.axvline(x=calc_k(35, L2), ls='--', color='grey', lw=0.5, alpha=0.4)
plt.axvline(x=-calc_k(41, L2), ls='-', color='grey', lw=0.5)
plt.axvline(x=calc_k(41, L2), ls='-', color='grey', lw=0.5)
plt.axvline(x=-calc_k(47, L2), ls='--', color='grey', lw=0.5, alpha=0.4)
plt.axvline(x=calc_k(47, L2), ls='--', color='grey', lw=0.5, alpha=0.4)
plt.fill_between(x_gap2, x1[peakind_1[10]]*np.ones(len(x_gap2)), x1[peakind_1[11]]*np.ones(len(x_gap2)), color='grey', alpha=0.2)
plt.fill_between(x_gap2, x2[peakind_2[10]]*np.ones(len(x_gap2)), x2[peakind_2[11]]*np.ones(len(x_gap2)), color='grey', alpha=0.4)
plt.fill_between(x_gap2, x1[peakind_1[22]]*np.ones(len(x_gap2)), x1[peakind_1[23]]*np.ones(len(x_gap2)), color='grey', alpha=0.2)
plt.fill_between(x_gap2, x2[peakind_2[22]]*np.ones(len(x_gap2)), x2[peakind_2[23]]*np.ones(len(x_gap2)), color='grey', alpha=0.4)

plt.fill_between(x_gap2, x2[peakind_2[5]]*np.ones(len(x_gap2)), x2[peakind_2[6]]*np.ones(len(x_gap2)), color='red', alpha=0.4)
plt.fill_between(x_gap2, x2[peakind_2[17]]*np.ones(len(x_gap2)), x2[peakind_2[18]]*np.ones(len(x_gap2)), color='red', alpha=0.4)
plt.fill_between(x_gap2, x2[peakind_2[29]]*np.ones(len(x_gap2)), x2[peakind_2[30]]*np.ones(len(x_gap2)), color='red', alpha=0.4)
plt.fill_between(x_gap2, x2[peakind_2[39]]*np.ones(len(x_gap2)), x2[peakind_2[40]]*np.ones(len(x_gap2)), color='red', alpha=0.4)
plt.legend()
plt.savefig('../../../comp_structure.png')


data3 = np.genfromtxt('Uebersichtsspektrum_5_Zellen_5u7,5cm_0-12000_Ringe16mm.dat')
x3 = data3[0:,0]
y3 = data3[0:,1]
pi3, _ = sp.find_peaks(y3, height=0.5, prominence=0.02)
pi3 = pi3[4:]
pi3 = np.concatenate([pi3[0:33], [790, 920], pi3[33:36], [946, 1032], pi3[36:39], [1069, 1149], pi3[39:], [1177]])
n3 = np.linspace(1, len(pi3), len(pi3))
print(pi3)
print(len(pi3))

#plt.plot(x3,y3)
#plt.plot(x3[pi3], y3[pi3], ls='', marker='.')
plt.figure(3)
plt.xlabel(r'Wellenzahl $k$ [m$^{-1}$]')
plt.ylabel(r'Frequenz $f$ [Hz]')
plt.plot(calc_k(n3, L3), x3[pi3], ls='', marker='.', color='b')
plt.plot(-calc_k(n3, L3), x3[pi3], ls='', marker='.', color='b')
plt.plot(calc_k(n_t3_, L3), x3[pi3], -calc_k(n_t3_, L3),x3[pi3], marker='.', ls='', color='b')
plt.axvline(x=calc_k(0, L3), ls='--', color='grey', lw=0.5, alpha=0.4)
plt.axvline(x=calc_k(9, L3), ls='-', color='grey', lw=0.5)
plt.axvline(x=-calc_k(9, L3), ls='-', color='grey', lw=0.5)
plt.axvline(x=calc_k(19, L3), ls='--', color='grey', lw=0.5, alpha=0.4)
plt.axvline(x=-calc_k(19, L3), ls='--', color='grey', lw=0.5, alpha=0.4)
plt.axvline(x=calc_k(29, L3), ls='-', color='grey', lw=0.5)
plt.axvline(x=-calc_k(29, L3), ls='-', color='grey', lw=0.5)
plt.axvline(x=calc_k(39, L3), ls='--', color='grey', lw=0.5, alpha=0.4)
plt.axvline(x=-calc_k(39, L3), ls='--', color='grey', lw=0.5, alpha=0.4)
plt.axvline(x=calc_k(49, L3), ls='-', color='grey', lw=0.5)
plt.axvline(x=-calc_k(49, L3), ls='-', color='grey', lw=0.5)
plt.fill_between(x_gap3, x3[pi3[8]]*np.ones(len(x_gap3)), x3[pi3[9]]*np.ones(len(x_gap3)), color='grey', alpha=0.2)
plt.fill_between(x_gap3, x3[pi3[13]]*np.ones(len(x_gap3)), x3[pi3[14]]*np.ones(len(x_gap3)), color='grey', alpha=0.2)
plt.fill_between(x_gap3, x3[pi3[18]]*np.ones(len(x_gap3)), x3[pi3[19]]*np.ones(len(x_gap3)), color='grey', alpha=0.2)
plt.fill_between(x_gap3, x3[pi3[23]]*np.ones(len(x_gap3)), x3[pi3[24]]*np.ones(len(x_gap3)), color='grey', alpha=0.2)
plt.fill_between(x_gap3, x3[pi3[33]]*np.ones(len(x_gap3)), x3[pi3[34]]*np.ones(len(x_gap3)), color='grey', alpha=0.2)
plt.fill_between(x_gap3, x3[pi3[38]]*np.ones(len(x_gap3)), x3[pi3[39]]*np.ones(len(x_gap3)), color='grey', alpha=0.2)
plt.fill_between(x_gap3, x3[pi3[43]]*np.ones(len(x_gap3)), x3[pi3[44]]*np.ones(len(x_gap3)), color='grey', alpha=0.2)
plt.savefig('../../../combined_solids.png')
plt.show()
