import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sp
from scipy.optimize import curve_fit
import scipy.odr

from uncertainties import unumpy

def calc_k(n, L):
    return n*np.pi/L

L1 = 0.4
L2 = 0.6
L3 = 0.625

n_t1 = np.array([1,2,3,4,5,6,7,8,9,10,11,11,10,9,8,7,6,5,4,3,2,1,1,2,3,4,5,6,7,8,9,10,11,9,8,7,6,5,4,3,2,1])

x_gap3 = np.linspace(-calc_k(49, L3), calc_k(49, L3), 5000)

data = np.genfromtxt('Uebersichtsspektrum_Defekt_1.dat')
x = data[0:,0]
y = data[0:,1]
pi, _ = sp.find_peaks(y, height=0.2, prominence=0.03)
pi = pi[3:]
index = [3,4,5,6,16,17]
pi = np.delete(pi, index)
pi = np.concatenate([pi[0:22], [663], pi[22:]])
n = np.linspace(1, len(pi), len(pi))
for i in range(22,33):
    n[i] = n[i] + 1
for i in range(33,42):
    n[i] = n[i] + 3

print(pi)
print(n)

plt.plot(x,y)
plt.plot(x[pi], y[pi], ls='', marker='.')
plt.figure(2)
plt.xlabel(r'Wellenzahl $k$ [m$^{-1}$]')
plt.ylabel(r'Frequenz $f$ [Hz]')
plt.plot(calc_k(n, L3), x[pi], ls='', marker='.', color='b')
plt.plot(-calc_k(n, L3), x[pi], ls='', marker='.', color='b')
plt.plot(calc_k(n_t1, L3), x[pi], -calc_k(n_t1, L3),x[pi], marker='.', ls='', color='b')
plt.axvline(x=calc_k(0, L3), ls='--', color='grey', lw=0.5, alpha=0.4)
plt.axvline(x=calc_k(11, L3), ls='-', color='grey', lw=0.5)
plt.axvline(x=-calc_k(11, L3), ls='-', color='grey', lw=0.5)
plt.axvline(x=calc_k(23, L3), ls='--', color='grey', lw=0.5, alpha=0.4)
plt.axvline(x=-calc_k(23, L3), ls='--', color='grey', lw=0.5, alpha=0.4)
plt.axvline(x=calc_k(35, L3), ls='-', color='grey', lw=0.5)
plt.axvline(x=-calc_k(35, L3), ls='-', color='grey', lw=0.5)
plt.axvline(x=calc_k(47, L3), ls='--', color='grey', lw=0.5, alpha=0.4)
plt.axvline(x=-calc_k(47, L3), ls='--', color='grey', lw=0.5, alpha=0.4)
plt.fill_between(x_gap3, x[pi[10]]*np.ones(len(x_gap3)), x[pi[12]]*np.ones(len(x_gap3)), color='grey', alpha=0.2)
plt.fill_between(x_gap3, x[pi[21]]*np.ones(len(x_gap3)), x[pi[22]]*np.ones(len(x_gap3)), color='grey', alpha=0.2)
plt.fill_between(x_gap3, x[pi[32]]*np.ones(len(x_gap3)), x[pi[33]]*np.ones(len(x_gap3)), color='grey', alpha=0.2)
plt.savefig('../../../defect.png')
