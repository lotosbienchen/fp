import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sp
from scipy import stats
from scipy.optimize import curve_fit
import scipy.odr
import math
from  analytic_wfm import peakdetect


from uncertainties import unumpy

def theta( alpha ):
    return math.degrees(np.arccos(np.cos(math.radians(alpha))/2-1/2))

plt.ion()



data = np.loadtxt('./07.12.2018/Kugelresonator/Peak5000/Kugelresonator_Peak5000_0grad.dat')
data2 = np.loadtxt('./07.12.2018/Kugelresonator/Peak5000/Kugelresonator_Peak5000_20grad.dat')
data3 = np.loadtxt('./07.12.2018/Kugelresonator/Peak5000/Kugelresonator_Peak5000_40grad.dat')

x=data[:,0]
y=data[:,1]

x2=data2[:,0]
y2=data2[:,1]

x3=data3[:,0]
y3=data3[:,1]

f = plt.figure()
plt.plot(x, y, linestyle='-', label=r'α=0°')
plt.plot(x2, y2, linestyle='-', label=r'α=20°')
plt.plot(x3, y3, linestyle='-', label=r'α=40°')    
plt.xlabel('Frequenz [Hz]')
plt.ylabel('Intensität')
plt.legend()



resonance=['./07.12.2018/Kugelresonator/Resonanzfrequenzen/Kugelresonator_Spektrum_180grad.dat',
'./07.12.2018/Kugelresonator/Resonanzfrequenzen/Kugelresonator_Spektrum_150grad.dat',
'./07.12.2018/Kugelresonator/Resonanzfrequenzen/Kugelresonator_Spektrum_120grad.dat',
'./07.12.2018/Kugelresonator/Resonanzfrequenzen/Kugelresonator_Spektrum_90grad.dat',
'./07.12.2018/Kugelresonator/Resonanzfrequenzen/Kugelresonator_Spektrum_60grad.dat',
'./07.12.2018/Kugelresonator/Resonanzfrequenzen/Kugelresonator_Spektrum_30grad.dat',
'./07.12.2018/Kugelresonator/Resonanzfrequenzen/Kugelresonator_Spektrum_0grad.dat']


f = plt.figure()

i=120
phi=180


for file in resonance:


    data = np.loadtxt(file)

    x=data[:,0]
    y=data[:,1]
    y=y*0.5+i
    plt.plot(x, y, linestyle='-', label='α='+str(phi)+' °')
    i=i-20
    phi=phi-30



plt.ylabel('Intensität mit bel. Verschiebung')
plt.xlabel('Frequenz [Hz]')
plt.legend()
plt.savefig('resonanz_winkel.pdf')



#plt.plot(x, y, linestyle='-', label='0 Grad')
#plt.plot(x2, y2, linestyle='-', label='20 Grad')
#plt.plot(x3, y3, linestyle='-', label='40 Grad')

#plt.legend()


data = np.loadtxt('./07.12.2018/Kugelresonator/Symmetriebrechung/Kugelresonator_Spektrum_HR_3mm_spacer.dat')
data2 = np.loadtxt('./07.12.2018/Kugelresonator/Symmetriebrechung/Kugelresonator_Spektrum_HR_6mm_spacer.dat')
data3 = np.loadtxt('./07.12.2018/Kugelresonator/Symmetriebrechung/Kugelresonator_Spektrum_HR_9mm_spacer.dat')




x=data[:,0]
y=data[:,1]


f = plt.figure('3mm Abstandshalter')
plt.plot(x,y, linestyle='-', label='3mm Abstandshalter')

peaks = peakdetect(y, x, lookahead=52)
s= [0]
s.append(peaks[0][1][0]-peaks[0][0][0])

plt.ylabel('Intensität')
plt.xlabel('Frequenz [Hz]')
plt.savefig('3mmspacer.pdf')


x2=data2[:,0]
y2=data2[:,1]


f = plt.figure('6mm Abstandshalter')
plt.plot(x2,y2, linestyle='-', label='6mm Abstandshalter')

peaks = peakdetect(y2, x2, lookahead=52)
s.append(peaks[0][1][0]-peaks[0][0][0])

plt.ylabel('Intensität')
plt.xlabel('Frequenz [Hz]')
plt.savefig('6mmspacer.pdf')


x3=data3[:,0]
y3=data3[:,1]

f = plt.figure('9mm Abstandshalter')
plt.plot(x3,y3, linestyle='-', label='9mm Abstandshalter')

plt.ylabel('Intensität')
plt.xlabel('Frequenz [Hz]')
plt.savefig('9mmspacer.pdf')

peaks = peakdetect(y3, x3, lookahead=60)
s.append(peaks[0][1][0]-peaks[0][0][0])




d=np.array([0,3,6,9])


f = plt.figure()

slope, intercept, r_value, p_value, std_err = stats.linregress(d,s)
line = slope*d+intercept

plt.plot(d, s, '+')
plt.plot(d, line, linestyle='-')
plt.ylabel('Peakabstand [Hz]')
plt.xlabel('Abstandshalterdicke [mm]')
plt.savefig('9mmspacer.pdf')




input("Press Enter to close...")
plt.close('all')