import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sp
from scipy.optimize import curve_fit
import scipy.odr

from uncertainties import unumpy

data = np.genfromtxt('Peakspektrum_25cm.dat')
print(data)

x = data[0:,0]
y = data[0:,1]

peakind, _ = sp.find_peaks(y, height=10, prominence=0.6)

n = np.linspace(1,len(peakind), len(peakind))
print(n)

def linear_fit(B, x):
    return B[0]*x/2/0.25 + B[1]

data_linearfit = scipy.odr.RealData(n[7:], x[peakind[7:]])
linear_model = scipy.odr.Model(linear_fit)
odr_linear = scipy.odr.ODR(data_linearfit, linear_model, beta0=[343, -4000])
odr_linear_out = odr_linear.run()
beta1 = unumpy.uarray(odr_linear_out.beta, odr_linear_out.sd_beta)
print(beta1)

n_new = np.linspace(7, len(peakind), 1000)
fitted_v = linear_model.fcn(odr_linear_out.beta, n_new)

# plf, clf = curve_fit(linear_fit, n[8:], x[peakind[8:]], p0=[346,6])
# print(plf)
# c, a = plf
#
# n_new = np.linspace(6, len(peakind) - 1, 1000)
#
# fitted_v = linear_fit(n_new, c, a)

#plt.plot(x, y, marker='.', ls='-')
#plt.plot(x[peakind1], y[peakind1], marker='.', ls='')
plt.figure(1)
plt.xlabel(r'$n$')
plt.ylabel(r'Frequenz $f$ [Hz]')
plt.plot(n, x[peakind], marker='.', ls='', label='Messpunkte')
plt.plot(n_new, fitted_v, label='linearer Fit')
plt.legend()
plt.savefig('/home/l0be/lotosbienchen/TU/Physik/FS 7/FP/QA/roehre_schall.png')

fitdata1 = np.genfromtxt('Peakfit_7,5cm_function.dat')
fitdata2 = np.genfromtxt('Peakfit_7,5cm_function_2.dat')

x_fit1 = fitdata1[0:,0]
y_fit1 = fitdata1[0:,1]

x_fit2 = fitdata2[0:,0]
y_fit2 = fitdata2[0:,1]

plt.figure(2)

plt.xlabel('Frequenz $f$ [Hz]')
plt.ylabel('Amplitude')
plt.plot(x_fit1, y_fit1, ls='-.', marker='x', color='black', label='Fit 1')
plt.plot(x_fit2, y_fit2, ls='--', marker='+', color='r', label='Fit 2')
plt.legend()
plt.savefig('/home/l0be/lotosbienchen/TU/Physik/FS 7/FP/QA/fitcompare.png')
