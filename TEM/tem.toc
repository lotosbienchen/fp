\babel@toc {ngerman}{}
\contentsline {section}{\numberline {1}Versuchsdurchf\IeC {\"u}hrung}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Versuchsziel}{2}{subsection.1.1}
\contentsline {section}{\numberline {2}Theoretischer Hintergrund}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Aufbau eines TEM und Strahlengang}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Elektronenbilder im TEM}{4}{subsection.2.2}
\contentsline {section}{\numberline {3}Messung}{5}{section.3}
\contentsline {subsection}{\numberline {3.1}\IeC {\"U}bungen zur Fokussierung}{5}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Messungen zu Goldinseln}{7}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Messungen zum Molybd\IeC {\"a}noxidkristall}{13}{subsection.3.3}
\contentsline {subsubsection}{\numberline {3.3.1}Aufnahmen zur Elektronenbeugung}{14}{subsubsection.3.3.1}
