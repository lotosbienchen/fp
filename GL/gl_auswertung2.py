import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from  analytic_wfm import peakdetect
from uncertainties import unumpy
from uncertainties import ufloat
from uncertainties.umath import *  # sin(), etc.
import scipy.odr
from lmfit.models import LorentzianModel
from scipy.signal import hilbert


def lorentz_fn(G, x):
   return 1/(np.pi*2) * G[0]/((x)**2+(G[0]/2)**2)

def fn(B, x):
   return B[0] * np.exp(-B[1] * x) + [2]

def lorentz_fit(x,y,w,p):
    x_fit=x-p
    indices=np.where(np.logical_and(x_fit < w, x_fit > -w))
    x_fit=x_fit[indices]
    print(x_fit)
    y_fit=y[indices]
    print(y_fit)
    mod = LorentzianModel()
    pars = mod.guess(y_fit, x=x_fit)
    out = mod.fit(y_fit, pars, x=x_fit)
    print(out.fit_report(min_correl=0.25))

    #lorentz_m=scipy.odr.Model(fn)
    #data=scipy.odr.RealData(x_fit,y_fit)
    #odr = scipy.odr.ODR(data, lorentz_m, beta0=[0.1, 0.1, 0.1])
    #odr_out=odr.run()
    #G = curve_fit(lorentz_fn,x_fit,y_fit)
    #G=odr_out.beta
    #odr_out.pprint()
    #plt.plot(x_fit,y_fit)
    #plt.plot(x_fit, fn(G, x_fit))
    #plt.show
    G=1
    return G, indices







d = ufloat(0.074, 0.005)
c=299792458

fsr=c/(2*d)

data = np.loadtxt('./PeterLukas/fp_data3_comlaser.txt', skiprows=1)
data2 = np.loadtxt('./PeterLukas/fp_data_initial_openlaser_82cm.txt', skiprows=1)
data3 = np.loadtxt('./PeterLukas/fp_data_openlaser_60cm.txt', skiprows=1)
data4 = np.loadtxt('./PeterLukas/fp_data_polefilter_190_comlaser.txt', skiprows=1)
data5 = np.loadtxt('./PeterLukas/fp_data_polefilter_280_comlaser.txt', skiprows=1)

x=data[:,0]
y=data[:,1]

x2=data2[:,0]
y2=data2[:,1]

x3=data3[:,0]
y3=data3[:,1]

x4=data4[:,0]
y4=data4[:,1]

x5=data5[:,0]
y5=data5[:,1]



peaks = peakdetect(y, x, lookahead=10)
fsr_t=(peaks[0][9][0]-peaks[0][5][0])/2
a=fsr/(fsr_t)
#G, indices =lorentz_fit(x,y,0.0001,peaks[0][5][0])
f1 = plt.figure(1)
plt.plot(x, y, linestyle='-')

print('com')
print('modenabstand in MHz')
abstandcom = peaks[0][6][0]-peaks[0][5][0]
print('{:10.2f}'.format(a*(peaks[0][6][0]-peaks[0][5][0])/1e6))
print(peaks[0][6][0]-peaks[0][5][0])
print(c/(2*a*(peaks[0][6][0]-peaks[0][5][0])))


for p in peaks[0]:
    plt.axvline(p[0])

plt.ylabel(r'$Spannung\left[V\right]$', rotation=0, labelpad=30)
plt.xlabel(r'$Zeit\left[s\right]$')

f2 = plt.figure(2)
peaks = peakdetect(y2, x2, lookahead=10)
plt.plot(x2, y2, linestyle='-')

fsr_t=(peaks[0][2][0]-peaks[0][6][0])
a=fsr/(fsr_t)

print('80cm')
print(peaks[0][1][0]-peaks[0][2][0])
print('modenabstand in MHz')
print('{:10.2f}'.format(a*(peaks[0][1][0]-peaks[0][2][0])/1e6))
print('theoretisch')
print('{:10.2f}'.format(c/(2*ufloat(0.8,0.005))/1e6))

print('com_neu')
print('modenabstand in MHz')
abstandcom += 1
print('{:10.2f}'.format(a*(abstandcom )/1e6))
print(c/(2*a*(abstandcom)))

#print(c/(2*a*(peaks[0][1][0]-peaks[0][2][0])))

for p in peaks[0]:
    plt.axvline(p[0])

plt.ylabel(r'$Spannung\left[V\right]$', rotation=0, labelpad=30)
plt.xlabel(r'$Zeit\left[s\right]$')


f3 = plt.figure(3)
peaks = peakdetect(y3, x3, lookahead=10)
plt.plot(x3, y3, linestyle='-')

print('60cm')
print(peaks[0][3][0]-peaks[0][4][0])
print(peaks[0][3][0]-peaks[0][2][0])
print('modenabstand in MHz')
print('{:10.2f}'.format(a*(peaks[0][3][0]-peaks[0][4][0])/1e6))
print('{:10.2f}'.format(a*(peaks[0][3][0]-peaks[0][2][0])/1e6))
print('theoretisch')
print('{:10.2f}'.format(c/(2*ufloat(0.6,0.005))/1e6))

#print(c/(2*a*(peaks[0][3][0]-peaks[0][2][0])))

for p in peaks[0]:
    plt.axvline(p[0])

plt.ylabel(r'$Spannung\left[V\right]$', rotation=0, labelpad=30)
plt.xlabel(r'$Zeit\left[s\right]$')

w=0.0003

p=peaks[0][3][0]
x_fit=x3-p
indices=np.where(np.logical_and(x_fit < w, x_fit > -w))
x_fit=x_fit[indices]
y_fit=y3[indices]

analytic_signal = hilbert(y_fit)
amplitude_envelope = np.abs(analytic_signal)

peaks = peakdetect(y_fit, x_fit, lookahead=10)
x_peaks=np.ones(len(peaks[0]))
y_peaks=np.ones(len(peaks[0]))
i=0
for p in peaks[0]:
    x_peaks[i]=p[0]
    y_peaks[i]=p[1]
    i += 1

# Define model function to be used to fit to the data above:
def gauss(x, *p):
    A, mu, sigma = p
    return A*np.exp(-(x-mu)**2/(2.*sigma**2))

# p0 is the initial guess for the fitting coefficients (A, mu and sigma above)
p0 = [10, 10, 10]

coeff, var_matrix = curve_fit(gauss, x_fit*10000, amplitude_envelope*10, p0=p0)


trialX = np.linspace(x_fit[0],x_fit[-1],1000)

# Fit a polynomial
fitted = np.polyfit(x_fit, y_fit, 10)[::-1]
y = np.zeros(len(trialX))
for i in range(len(fitted)):
   y += fitted[i]*trialX**i


def func(x,a,b,c):
   return a*np.exp(-b*x)-c

# Fit an exponential
#popt, pcov = curve_fit(func, x_fit, y_fit, diag=(1./x_fit.mean(),1./y_fit.mean()))
#fit = func(trialX, *popt)

# Get the fitted curveä
coeff=coeff+0.1
coeff += 0.05
coeff[1]=-0.55
coeff[2] += 0.25
fit = gauss(trialX*10000, *coeff)/10





f3 = plt.figure(4)
plt.plot(x_fit, y_fit, linestyle='-')
#plt.plot(x_fit, amplitude_envelope, linestyle='-')
plt.plot(trialX, fit, linestyle='-')
#plt.plot(x_peaks, y_peaks, linestyle='-')
plt.ylabel(r'$Spannung\left[V\right]$', rotation=0, labelpad=30)
plt.xlabel(r'$Zeit\left[s\right]$')

f5 = plt.figure(5)

plt.plot(x4, y4, linestyle='-', label='Polfilter bei 190 Grad')
plt.plot(x5, y5, linestyle='-', label='Polfilter bei 280 Grad')
plt.legend()

plt.ylabel(r'$Spannung\left[V\right]$', labelpad=5)
plt.xlabel(r'$Zeit\left[s\right]$')

#plt.show(f5)
