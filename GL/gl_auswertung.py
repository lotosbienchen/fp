import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sp
from scipy.optimize import curve_fit
import scipy.odr

from uncertainties import unumpy

"""
Berechnung des Stabilitätsparameters in unserem Versuch und Erstellung des
Stabilitätskurvenplots mit Einzeichnung unsere Stabilitätsparameters
"""
def calc_stability(L, R):
    """Stabilitätsbedingung"""
    return 1 - L/R

g1 = np.linspace(-4, 4, 1000)
g2 = np.linspace(-4, 4, 1000)
L = np.linspace(0,1500,1500)
R1 = 10e10
R2 = 1000

g1_exp = calc_stability(L, R1)
g2_exp = calc_stability(L, R2)

g1_stab = []
g2_stab = []

y = 0
for i in range(len(g1)):
    for j in range(len(g2)):
        if 0.995 <= g1[i]*g2[j] <=1.005:
            g1_stab.append(g1[i])
            g2_stab.append(g2[j])

f1 = plt.figure(1)
plt.xlabel(r'$g_1$')
plt.ylabel(r'$g_2$')
plt.plot(g1_stab, g2_stab, marker='.', color='black', linestyle='', ms=1)
plt.plot([0,0],[-4,4],[-4,4],[0,0], color='black')
plt.fill_betweenx(g1_stab, g2_stab, color='b')
plt.fill_between(g1_stab, g2_stab, color='b')
plt.plot(g1_exp, g2_exp, marker='.', color='red', ms=1,
         label='Laserkonfiguration in L-Abhängigkeit')
plt.plot(calc_stability(1000, R1), calc_stability(1000, R2), marker='x', ms=10,
         color='r', label='Grenze für Stabilitätsbedingung')
plt.legend()
plt.savefig('stabilitaet.png')

"""
Berechnung der Verstärkung
"""
P = unumpy.uarray([50.5, 821, 811], [0.917, 40.2, 58.9])
P1 = P[1] - P[0]
P2 = P[2] - 10
P3 = P2/P1
print('===')
print(P3)

"""
Berechnungen zum offenen Laser
"""

L = unumpy.uarray([610, 650, 700, 760, 620, 890, 950, 1000, 1005], 3)
I = unumpy.uarray([1440, 1500, 1500, 1330, 1409, 1500, 1370, 530, 0],
                  [12, 5.7, 4.3, 3.2, 4.1, 5.3, 4.5, 2.6, 0])

f2 = plt.figure(2)

plt.xlabel('Resonatorlänge L[mm]')
plt.ylabel(r'Leistung P[$\mu$W]')
plt.errorbar(unumpy.nominal_values(L), unumpy.nominal_values(I),
             xerr=unumpy.std_devs(L), yerr=unumpy.std_devs(I),
             linestyle='', marker='+', capsize=3, label='Messwerte')
plt.legend()
plt.savefig('abhaengigkeit.png')

f3 = plt.figure(3)
a = unumpy.uarray([0, 20, 40, 60, 80, 100, 120, 140, 160,
                   180, 200, 220, 240, 260, 280, 300, 320, 340], 2)
I_a = unumpy.uarray([1230, 1180, 859, 449, 104, 28,
                     226, 699, 1030, 1260, 1200, 910,
                     463, 122, 26, 232, 618, 1030], [1.3, 2.0, 2.5, 0.5, 0.2,
                                                     0.033, 0.163, 0.9, 6.6,
                                                     2.5, 2.1, 1.6, 0.8, 0.3,
                                                     0.127, 2.242, 1.1, 4.0])

def fit_function(B, x):
    """
    Funktion zum Fit der sin^2-Abhängigkeit der Leistung vom Polarisations-
    winkel, wie sie von Malus Law vorhergesagt wird
    """
    return B[0]*(np.sin(np.pi*(x-B[1])/180))**2 + B[2]

fitdata = scipy.odr.RealData(unumpy.nominal_values(a),
                             unumpy.nominal_values(I_a), sx=unumpy.std_devs(a),
                             sy=unumpy.std_devs(I_a))
fitmodel = scipy.odr.Model(fit_function)

odr_fit = scipy.odr.ODR(fitdata, fitmodel, beta0=[1000., 90, 0.00])
odr_fit_out = odr_fit.run()

beta = unumpy.uarray(odr_fit_out.beta, odr_fit_out.sd_beta)

x_new = np.linspace(0, 360, 360)

y_new = fitmodel.fcn(odr_fit_out.beta, x_new)
y_new_dw=fitmodel.fcn(unumpy.nominal_values(beta)-unumpy.std_devs(beta), x_new)
y_new_up=fitmodel.fcn(unumpy.nominal_values(beta)+unumpy.std_devs(beta), x_new)

plt.ylabel(r'Leistung P[$\mu$W]')
plt.xlabel(r'Polarisationswinkel $\alpha$[°]')
plt.plot(x_new, y_new, label=r'$\sin^2$-Fit')
plt.errorbar(unumpy.nominal_values(a), unumpy.nominal_values(I_a),
             xerr=unumpy.std_devs(a),
             yerr=unumpy.std_devs(I_a), linestyle='', marker='+', capsize=3,
             label='Messwerte')

plt.legend()
plt.savefig('malus.png')

# Kaustik
plt.figure(4)
kaustikdata = np.genfromtxt('kaustik.csv', delimiter=',', skip_header=2)

L = unumpy.uarray(kaustikdata[0:10,0], 5)
w = kaustikdata[0:10,6]
L_theo = kaustikdata[0:,7]
w_theo = kaustikdata[0:,8]
print(L)

def kaustikfit(B, z):
    return B[0]*np.sqrt(1 + ((z-B[1])/(np.pi/B[2]*B[0]**2))**2)

def kaustikfit1(z, w0, z0, la):
    return w0*np.sqrt(1 + ((z-z0)/(np.pi/la*w0**2))**2)

data_kaustikfit = scipy.odr.RealData(unumpy.nominal_values(L), w,
                                     sx=unumpy.std_devs(L))
kaustikmodel = scipy.odr.Model(kaustikfit)
odr_kaustik = scipy.odr.ODR(data_kaustikfit, kaustikmodel,
                            beta0=[0.05, 171, 0.000632])
odr_kaustik_out = odr_kaustik.run()
beta1 = unumpy.uarray(odr_kaustik_out.beta, odr_kaustik_out.sd_beta)
print(beta1)

kopt, kcov = curve_fit(kaustikfit1, unumpy.nominal_values(L), w,
                       p0=[171, 0.000631992, 0.05])
w0, z0, la = kopt
print(kopt)

x_kaustik = np.linspace(np.min(L_theo), np.max(L_theo), 1000)
y_kaustik = kaustikmodel.fcn(odr_kaustik_out.beta, x_kaustik)
y1_kaustik = kaustikfit1(x_kaustik, w0, z0, la)

#plt.plot(unumpy.nominal_values(L), w, linestyle='', marker='x')
plt.xlabel(r'Abstand  x[mm] Linse zu Kamera')
plt.ylabel('mittlerer Bündelradius w[mm]')
plt.errorbar(unumpy.nominal_values(L), w, xerr=unumpy.std_devs(L),
             linestyle='', marker='+', capsize=3, label='Messung')
plt.plot(L_theo, w_theo, linestyle='--',
         label='Theoretisch erwartbarer Verlauf')
plt.plot(x_kaustik, y_kaustik, label='Fit der Messwerte')
#plt.plot(x_kaustik, y1_kaustik)
plt.legend()
plt.savefig('kaustik.png')

# Faserspektrometer
plt.figure(5)
data = np.genfromtxt('data_faser.txt')
l = data[0:,0]
I_l = data[0:,1]

peakind, _ =sp.find_peaks(I_l, height=14000)

a = np.int(peakind) - 20
b = np.int(peakind) + 20
I_l = data[0:,1]/I_l[peakind]

x = l[a:b]
y = I_l[a:b]

mean = sum(x * y) / sum(y)
sigma = np.sqrt(sum(y * (x - mean)**2) / sum(y))

def gaussian_fit(x, a, x0, sigma, b):
    return a * np.exp(-(x - x0)**2 / (2 * sigma**2)) + b

popt,pcov = curve_fit(gaussian_fit, x, y, p0=[max(y), mean, sigma, 0.005])
A, x0, sigma, B = popt

fwhm = np.abs(sigma)*2*np.sqrt(2*np.log(2))
print(fwhm)

x_gauss = np.linspace(np.int(x[3]), np.int(x[-1]), 1000)
y_gauss = gaussian_fit(x_gauss, A, x0, sigma, B)
peakind, _ = sp.find_peaks(y_gauss, height=0.8)

dv = 2.997e8/(2*0.815)/(10**9)
print(dv)
lwv = x_gauss[peakind]
print(lwv)
print(2.997e8/(lwv*10**(-9))/(10**9)/(10**4))

print(lwv**2/(2*0.815)*10**(-10))

plt.xlabel(r'Wellenlänge $\lambda$[nm]')
plt.ylabel('rel. Intensität')
plt.plot(l[a:b], I_l[a:b], linestyle='', marker='x', label='Messwerte')
plt.plot(x_gauss, y_gauss, label='Gaussfit')
plt.legend()
plt.savefig('faserspektrometer.png')
plt.show()
