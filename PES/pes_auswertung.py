import numpy as np
import scipy.signal as sp
import matplotlib.pyplot as plt

# Abspeichern der Daten in festgelegter Variable
data = np.genfromtxt('pes.dat')

# Erste Spalte von data wird als E_kin abgespeichert
E_kin= data[:,0]
# Rest von data wird als Intensität abgespeichert und transformiert, damit die Dimensionen zur Darstellung übereinstimmen
Int = data[0:255, 1:].T

# Definiere Matrixlängen
Nx = len(E_kin)
Ny = len(Int[:,0])


print(len(Int[0,:]))

#Winkelarray
a = np.linspace(-1.8,9.0, Ny)
a[6] = 0.0
# Winkelarray im Bogenmaß
arad = a*np.pi/180

# Elektronenmasse
m = 9.109e-31
# red. planksches Wirkungsquantum
hbar = 4.135e-15
# Fermi-Energie
fermi_energy = 50.41

# Matrix für Peaks in Intensität
I_peak = np.zeros((Nx, Ny))

print(Int[2,:])

peakind = sp.find_peaks_cwt(Int[2,], np.arange(1,6))
print(E_kin[peakind])

def parallel_impulse(m, E_peak, hbar, arad):
    """
    Berechnung des parallelen Anteils der Elektronenimpulse
    """
    for a in E_peak:
        #print(a)
        p_p = np.sqrt(2*m*a)/hbar*np.sin(arad)
        #print(p_p)
    return p_p

def binding_energy(fermi_energy, E_kin):
    """
    Berechnung der Bindungsenergie
    """
    return(fermi_energy - E_kin)

e_bind = binding_energy(fermi_energy, E_kin)
p_p = parallel_impulse(m, E_kin, hbar, arad)
#print(e_bind)

#plotten
for i in range(Ny):
    plt.plot(E_kin, Int[1,])
    #Fermikante
    plt.axvline(x=fermi_energy, linestyle='-.', color='r')


plt.show()
