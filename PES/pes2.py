import numpy as np
import scipy.signal as sp
import matplotlib.pyplot as plt

# Abspeichern der Daten in festgelegter Variable
data = np.genfromtxt('pes.dat')

# Erste Spalte von data wird als E_kin abgespeichert
E_kin= data[:,0]
# Rest von data wird als Intensität abgespeichert und transformiert, damit die Dimensionen zur Darstellung übereinstimmen
Int = data[0:255, 1:].T

# Definiere Matrixlängen
Nx = len(E_kin)
Ny = len(Int[:,0])
k = 15
m = 6

#Winkelarray
a = np.linspace(-1.8,9.0, Ny)
a[6] = 0.0
# Winkelarray im Bogenmaß
arad = a*np.pi/180


# Elektronenmasse
m = 9.109e-31
# red. planksches Wirkungsquantum
hbar = 1.054e-34
# Fermi-Energie
max_energy = 50.41

e=1.602e-19

# Matrix für Peaks in Intensität
I_peak = np.zeros((Nx, Ny))

E_peak = np.zeros((Nx, Ny))

for i in range(Ny):
    #peakind, _ = sp.find_peaks(Int[i,], prominence=0.3)
    peakind = sp.find_peaks_cwt(Int[i,], np.arange(3,10), noise_perc=60)
    peakind = peakind[1:]
    for j in peakind:
        E_peak[j,i] = E_kin[j]



def parallel_impulse(m, E_peak, hbar, arad):
    """
    Berechnung des parallelen Anteils der Elektronenimpulse
    """
    p_p = np.sqrt(2*m*E_peak*e)/hbar*np.sin(arad)/10e8

    return p_p

def binding_energy(max_energy, E_kin):
    """
    Berechnung der Bindungsenergie
    """
    return(-max_energy + E_kin)

e_bind = binding_energy(max_energy, E_peak)
p_p = parallel_impulse(m, E_peak, hbar, arad)


k_parabel=np.array([-1.11, -0.73, -0.34, 0.028, 0.39, 0.97, 1.53, 1.71, 2.26, 2.63, 2.81, 3.2, 3.73])
E_bind_parabel = np.array([-1.6, -1.45, -1.35, -1.33, -1.34, -1.48, -1.76, -1.93, -2.11, -2.28, -2.34, -2.41, -2.45])

z = np.polyfit(k_parabel, E_bind_parabel, 8)

f = np.poly1d(z)

k_new = np.linspace(k_parabel[0], k_parabel[-1], 100)
E_new = f(k_new)

fig = plt.figure(figsize=(8,6))
ax1 = plt.subplot(111)
#plotten
for i in range(Ny):
    ax1.plot(E_kin, Int[i,], color='black')
    #Fermikante
    ax1.axvline(x=max_energy, linestyle='-.', color='r', linewidth=1)

    str = (" %.1f°" %a[i])
    ax1.text(E_kin[235], Int[i,254]+2.7, str, fontsize=7)

stri = ("E_fermi=50.41 eV")
ax1.text(E_kin[175], Int[0,175]+32, r'$E_{max}=50.41$ eV', fontsize=10, color='r')
ax1.set_yticks([])
ax1.set_xlabel(r'$E_{kin}}$ in eV')
ax1.set_ylabel('Intensität in bel. Einheit')
ax1.set_xlim(46.935, 50.745)
ax2 = ax1.twiny()
ax2.set_xlim(46.935, 50.745)
ax2.plot(E_kin, np.zeros(Nx)-5, color='white')

plt.show()
plt.figure(figsize=(8,6))

ax = plt.subplot(111)
ax.set_ylim(-3.5,0)
ax.plot(p_p, e_bind, linestyle='', marker='x', color='black')
ax.xaxis.tick_top()
ax.set_xlabel(r'parallele Impulskomponente $k_{||}$ in $10^9$ m$^{-1}$')
ax.xaxis.set_label_position('top')
ax.set_ylabel(r'$E_{bind}$')
#ax.plot(p_p, np.zeros(255)-0.65, color='g', linestyle='--', linewidth=1)
ax.axhline(y=-0.65, color='r', linestyle='--', linewidth=3)
ax.plot(k_new, E_new, linestyle='--', linewidth=3)


plt.show()
